<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<script src="../vendors/js/bootstrap.min.js"></script>  
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas - Consulta Funcionário </title>
</head>
<body>
	<div class="container">
		<?php include('../includes/headerRh.php'); ?>
		<div class="form-horizontal login-inicial">
			<div class="row">
				<!--<form class="form-horizontal" method="POST">-->
					<fieldset> 
						<legend>Recursos Humanos - Consulta de Funcionário</legend>
						<br>
                        <form action="../controllers/ConsultarFuncionario.php" method="POST">
                            <div>
                                <ul class="nav navbar-nav">
                                    <li><input type="text" id="text_busca" class="form-control col-lg-8" name="text_busca" style="width:468px;"placeholder="Pesquise pelo nome"></li>
                                    <li><button type="submit" id="iniciar_busca" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> </button></li>
                                </ul>
                            </div>           
                        </form>
                        <div class="form-group">
                        <!--div class="form-group" id="hidden_search"-->
                            <br><br><br><br>
                            <?php
                              if(array_key_exists("criado", $_SESSION) && ($_SESSION["criado"] == true)){
                                            echo '<div class="alert alert-success" role="alert">Usuário criado com sucesso! O login e Senha gerados automaticamente são identicos à matricula.</div>';
                                        }
                                        unset($_SESSION['criado']);
                            ?>
                            <?php
                              if(array_key_exists("editado", $_SESSION) && ($_SESSION["editado"] == true)){
                                            echo '<div class="alert alert-success" role="alert">Funcionário editado com sucesso!</div>';
                                        }
                                        unset($_SESSION['editado']);
                            ?>
                            <?php
                              if(array_key_exists("deletado", $_SESSION) && ($_SESSION["deletado"] == true)){
                                            echo '<div class="alert alert-success" role="alert">Funcionário deletado com sucesso.</div>';
                                        }
                                        unset($_SESSION['deletado']);
                            ?>
                            <?php
                              if(array_key_exists("user_deletado", $_SESSION) && ($_SESSION["user_deletado"] == true)){
                                            echo '<div class="alert alert-success" role="alert">Usuário deletado com sucesso.</div>';
                                        }
                                        unset($_SESSION['user_deletado']);
                            ?>
                            <?php 
                              if(isset($_SESSION['result'])){
                              $qtd = count($_SESSION['result']);
                              $result = $_SESSION['result'];                              
                              unset($_SESSION['result']);
                              if($qtd==0){
                                echo '
                                <div class="alert alert-dismissable alert-info funcInvalido"> 
                                  <button type="button" class="close" data-dismiss="alert">×</button>                         
                                  Usuário não encontrado
                                  <strong>
                                     <div id="texto_busca"></div>
                                  </strong>                                
                                </div>';
                              }
                              else{
                                echo '
                                  <table class="table table-striped table-hover">
                                    <thead>
                                      <tr>
                                        <th>#</th>
                                        <th>Matricula</th>
                                        <th>Nome</th>
                                        <th>Tipo do funcionário</th>
                                        <th>Visualizar</th>
                                      </tr>
                                    </thead>
                                    <tbody>';
                                $tipoFunc = array("Estagiário", "Funcionário", "Prestador de Serviço");
                                $_SESSION["lista_func"] = $result;    
                                for($i=0 ;$i<$qtd; $i++){                                  
                                  //$_SESSION["id"] = $i;
                                  echo '
                                    <tr>
                                      <td>'.(string)($i+1).'</td>
                                      <td>'.$result[$i]['inf_matricula'].'</td>
                                      <td>'.$result[$i]['inf_nome'].'</td>
                                      <td>'.$tipoFunc[$result[$i]['TipoFuncionario_tfn_id']-1].'</td>
                                      <td>                                
                                        <form onSubmit="return false;" method="POST">
                                          <input type="hidden" name="id" value="'.$i.'" aria-hidden>                                      
                                          <button type="submit" class="btn btn-info btn-xs vis_perfil" id="'.$i.'" data-target="#modal_dados_usuario" 
                                          data-toggle="modal"><span class="glyphicon glyphicon-eye-open"></span></button>
                                        </form>
                                      </td>
                                      <!--td-->
                                        <div class="hidden-information" id="info-'.$i.'">';
                                          include '../includes/visualizarFuncionario.php';
                                        echo '</div>
                                      <!--/td-->';
                                }
                                echo ' </table>
                                </table>';
                              }
                            }                            
                            ?>

                        </div>
					</fieldset>
					<!--</form>-->
			</div>
		</div>
		<?php include('../includes/footer.php'); ?>
	</div>
	<script src="../vendors/js/jquery-1.11.2.min.js"></script>
	<script src="../vendors/js/bootstrap.min.js"></script>
		
	

</body>
<div class="modal fade" id="modal_dados_usuario1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <?php 
    if (isset($_POST['chosen']))
    {
        $chosen = $_POST['chosen'];
        echo $chosen;
    }
   ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Visualizar Funcionário</h4>
      </div>
      <div class="modal-body">
        <?php
          if(isset($_POST)){
            var_dump($_POST);
          }
          //include '../iframes/visualizarFuncionario2.php';
          //<iframe src="../iframes/visualizarUsuario.php"></iframe>
        ?>        
      </div>
      <!--div class="modal-footer">
        <button type="button" id="btnRemoverUsuario" class="btn btn-danger">Remover</button>
        <button type="button" id="btnEditarUsuario" class="btn btn-primary">Editar</button>
        <button type="button" id="btnCriarUsuario" data-toggle="modal" class="btn btn-success">Criar Usuário</button>
      </div-->
    </div>
  </div>
</div>


<div class="modal fade" id="modal_cadastro_usuarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">
                  Criar conta de usuário:
              </h4>
          </div>
          <div class="modal-body">
              <div class="form-horizontal">
                  <div class="form-group">
                    <div class="form-group">
                        <label for="criar_user" style="text-align:center;" class="control-label">
                        Deseja criar uma conta de usuário do sistema para este funcionário?
                        </label>
                      </div>
                      <div class="form-group">
                        <label for="inputMatricula" class="col-lg-4 control-label">Matrícula</label>
                        <div class="col-lg-12">
                          <input type="text" class="form-control" id="inputMatricula" placeholder="AB0000" name="matricula" disabled="">
                        </div>
                      </div>
                  </div>                        
                  <div class="modal-footer">
                  <button id="btn_cancelar_criar" type="reset" data-dismiss="modal" name="btn_cancelar" class="btn btn-danger">
                      Não
                  </button>
                  <button id="btn_cadastrar_criar" type="button" runat="server" name="btn_cadastrar" class="btn btn-success">
                      Sim
                  </button>
              </div>
              </div>
          </div>
          <!-- form -->
      </div>
  </div>
</div>

<div class="modal fade" id="modal_confirmar_exclusaos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">
                  Excluir Funcionário
              </h4>
          </div>
          <div class="modal-body">
              <div class="form-horizontal">
                  <div class="form-group">
                    <div class="form-group">
                        <p for="criar_user" style="text-align:center;" class="control-label">
                        Deseja realmente excluir este funcionário?
                        </p>
                    </div>                    
                  </div>                        
                  <div class="modal-footer">
                  <button id="btn_cancelar_confirm" type="reset" data-dismiss="modal" name="btn_cancelar" class="btn btn-danger">
                      Não
                  </button>
                  <button id="btn_cadastrar_confirm" type="button" runat="server" name="btn_cadastrar" class="btn btn-success">
                      Sim
                  </button>
                  </div>
              </div>
          </div>
          <!-- form -->
      </div>
  </div>
</div>

  <script type="text/javascript">
    function mascara(t, mask){
      var i = t.value.length;
      var saida = mask.substring(1,0);
      var texto = mask.substring(i)
      if (texto.substring(0,1) != saida){
        t.value += texto.substring(0,1);
      }
    }
  </script>
  <script type="text/javascript">
      $(document).ready(function () {
          $("div#hidden_search").hide();
          $("button#iniciar_busca").click(function () {
              $("div#hidden_search").show();
          });
      });   
  </script>
  <script>
      $('#text_busca').on('keydown', function (event) {
          if (event.keyCode === 13) {
              $("div#hidden_search").show();
          }
      });
  </script>
  <script>
      $("#btnCriarUsuario").click(function() {
          $("#modal_dados_usuario").modal('hide');
          $("#modal_cadastro_usuario").modal('show');
      });
  </script>
  <script>
      $("#btnRemoverUsuario").click(function() {
          $("#modal_dados_usuario").modal('hide');
          $("#modal_confirmar_exclusao").modal('show');
      });
  </script>
  <script>
      $("#btn_cancelar_confirm").click(function() {
          $("#modal_dados_usuario").modal('show');
          $("#modal_confirmar_exclusao").modal('hide');
      });
  </script>
  <script>
      $("#btn_cancelar_criar").click(function() {
          $("#modal_dados_usuario").modal('show');
          $("#modal_confirmar_exclusao").modal('hide');
      });
  </script>
  <script type="text/javascript">  
      onClick="getId(this.id)"    
      function getId(clicked_id){
        myId = clicked_id;        
        alert(myId);
        $.data(document,"$chosen",myId);
      }      
  </script>
  <script>
    $(document).ready(function() {
        $(".hidden-information").hide();        
        $(".vis_perfil").click(function() {
            var chosen = $(this).attr('id');
            var element = document.getElementById("info-" +  chosen);
            $(".hidden-information").hide();
            $(".label-edit"+chosen).hide();
            $(element).toggle("slow","swing");
            /*$.ajax({
                type: "POST",
                url: 'consultarFuncionario.php',
                data: "chosen=" + chosen,
                success: function(data)
                {
                  //element.toggle("slow","swing");
                    alert(chosen + " success!");
                }
            });*/            
        });
        $(".btnUser").click(function() { 
          var str = $(this).attr('id');
          var pos = str.substr(3,4);
          var index = str.substr(15);//var index = str.substr(15,1);
          //alert(index);
          if(pos == "Rmvr"){
            $("#modal_confirmar_exclusao"+index).modal('show');
          } 
          else{
            if(pos == "Edit"){
              $(".label-read"+index).hide("slow","swing");
              $(".label-edit"+index).show("slow","swing");
            } 
            else{
              if(pos == "Cria"){
                $("#modal_cadastro_usuario"+index).modal('show');
              }else{
                if(pos == "Conf" || pos == "Canc"){
                  $(".label-edit"+index).hide("slow","swing");
                  $(".label-read"+index).show("slow","swing");
                } 
              }
            }
          }
        });
    });
  </script>
</html>