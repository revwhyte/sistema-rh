<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<script src="../vendors/js/bootstrap.min.js"></script>  
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas</title>
</head>
<body>
	<div class="container">
		<?php include('../includes/headerRh.php'); ?>
		<div class="form-horizontal login-inicial">
			<div class="row">
				<form class="form-horizontal" method="POST">
					<fieldset>
						<legend>Recursos Humanos</legend>
							<h4>Funcionários</h4>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/add.png" style="width:136px;height:136px;" alt="">
								<a href="../views/cadastrarFuncionario.php" class="btn btn-primary btn-lg btn-block">Cadastrar</a></div>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/view.png" style="width:136px;height:136px;" alt="">
								<a href="../views/consultarFuncionario.php" class="btn btn-primary btn-lg btn-block">Consultar</a></div>
							<h4>Usuários</h4>	
                                <div class="col-lg-12">As operações de usuário do sistema podem ser realizadas com uma busca do respectivo funcionário.</div>
					</fieldset>
					</form>
				</div>
			</div>
			<?php include('../includes/footer.php'); ?>
		</div>
		<script src="../vendors/js/jquery-1.11.2.min.js"></script>
		<script src="../vendors/js/bootstrap.min.js"></script>
	</body>
</html>