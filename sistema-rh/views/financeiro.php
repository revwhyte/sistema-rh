<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<script src="../vendors/js/bootstrap.min.js"></script>  
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas</title>
</head>
<body>
	<div class="container">
		<?php include('../includes/headerFin.php'); ?>
		<div class="form-horizontal login-inicial">
			<div class="row">
				<form class="form-horizontal" method="POST">
					<fieldset>
						<legend>Financeiro</legend>
							<h4>Contas a pagar</h4>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/add.png" style="width:136px;height:136px;" alt="">
								<a href="" class="btn btn-primary btn-lg btn-block">Cadastrar</a></div>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/view.png" style="width:136px;height:136px;" alt="">
								<a href="" class="btn btn-primary btn-lg btn-block">Consultar</a></div>
							<h4>Contas a receber</h4>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/add.png" style="width:136px;height:136px;" alt="">
								<a href="" class="btn btn-primary btn-lg btn-block">Cadastrar</a></div>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/view.png" style="width:136px;height:136px;" alt="">
								<a href="" class="btn btn-primary btn-lg btn-block">Consultar</a></div>
							<h4>Balanço</h4>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/add.png" style="width:136px;height:136px;" alt="">
								<a href="" class="btn btn-primary btn-lg btn-block">Cadastrar</a></div>
                                <div class="jumbotron col-lg-6">
                                <img src="../assets/images/view.png" style="width:136px;height:136px;" alt="">
								<a href="" class="btn btn-primary btn-lg btn-block">Consultar</a></div>
					</fieldset>
					</form>
				</div>
			</div>
			<?php include('../includes/footer.php'); ?>
		</div>
		<script src="../vendors/js/jquery-1.11.2.min.js"></script>
		<script src="../vendors/js/bootstrap.min.js"></script>
	</body>
</html>