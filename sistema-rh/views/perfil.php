<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas - Perfil</title>
</head>
<body>
	<div class="container">
		<?php 
			include('../includes/header_main.php'); 
			$user = $_SESSION['profile'][0];
			//var_dump($user);
		?>
		<div class="form-horizontal login-inicial">
			<div class="row">
					<fieldset>					
						<?php
							if(array_key_exists("senha_alterada", $_SESSION) && ($_SESSION["senha_alterada"] == true)){
				                echo '<div class="alert alert-success" role="alert">Senha alterada com sucesso.</div>';
				            }
				            unset($_SESSION['senha_alterada']);
						?>
						<?php
							if(array_key_exists("senha_incorreta", $_SESSION) && ($_SESSION["senha_incorreta"] == true)){
				                echo '<div class="alert alert-danger" role="alert">Senha incorreta!</div>';
				            }
				            unset($_SESSION['senha_incorreta']);
						?>
						<?php
							if(array_key_exists("senha_diferente", $_SESSION) && ($_SESSION["senha_diferente"] == true)){
				                echo '<div class="alert alert-danger" role="alert">Senha divergente!</div>';
				            }
				            unset($_SESSION['senha_diferente']);
						?>
						<?php
							if(array_key_exists("info_alterada", $_SESSION) && ($_SESSION["info_alterada"] == true)){
				                echo '<div class="alert alert-success" role="alert">Informações alteradas com sucesso.</div>';
				            }
				            unset($_SESSION['info_alterada']);
						?>
						<?php
							if(array_key_exists("banc_alterada", $_SESSION) && ($_SESSION["banc_alterada"] == true)){
				                echo '<div class="alert alert-success" role="alert">Informações bancárias alteradas com sucesso.</div>';
				            }
				            unset($_SESSION['banc_alterada']);
						?>
						<?php
							if(array_key_exists("spec_alterada", $_SESSION) && ($_SESSION["spec_alterada"] == true)){
				                echo '<div class="alert alert-success" role="alert">Informações Específicas alteradas com sucesso.</div>';
				            }
				            unset($_SESSION['spec_alterada']);
						?>
						<legend>Perfil</legend>
						<h4>Informações de usuário</h4>
						<form class="form-horizontal" action="../controllers/EditarPerfil.php" method="POST">
							<div class="form-group">
					          <label for="inputMatricula" class="col-lg-2 control-label">Matrícula (Login):</label>
					          <div class="col-lg-10">
					          	<?php 
						          	echo '
						            <label type="text" class="form-control show-info-user" readonly="" >'.$user['inf_matricula'].'</label>
						            <input type="text" class="form-control edit-info-user" value="'.$user['inf_matricula'].'" name="matricula" readonly="" >';
					             ?>
					          </div>
					        </div>
					        <div class="form-group">
					          <label for="inputMatricula" class="col-lg-2 control-label edit-info-user2">Senha:</label>
					          <div class="col-lg-10">
	                            <input type="password" class="form-control edit-info-user" placeholder="Senha atual" name="senha_confirmar_senha">
	                            <input type="password" class="form-control edit-info-user" placeholder="Senha nova" name="nova_senha">
	                            <input type="password" class="form-control edit-info-user" placeholder="Confirmar senha nova" name="confirmar_senha">
					          </div>
					        </div>
					        <br>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<input type="button" class="btn btn-primary show-info-user" id="editar-perfil-user" style="float:right;" value="Editar">
									<input type="submit" class="btn btn-success edit-info-user" id="editar-confirm-user" style="float:right;" value="Confirmar" name="confirm-user">
									<input type="button" class="btn btn-default edit-info-user" id="editar-cancel-user" style="float:right;" value="Cancelar">
								</div>
							</div>
						</form>
						<hr>
						<h4>Informações básicas</h4>
						<form class="form-horizontal" action="../controllers/EditarPerfil.php" method="POST">
							<div class="form-group">
								<label for="inputName" class="col-lg-2 control-label">Nome completo</label>
								<div class="col-lg-10">
									<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-basic">'.$user['inf_nome'].'</label>
												  <input type="text" class="form-control edit-info-basic" value="'.$user['inf_nome'].'" name="inf_nome">';
										}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputDate" class="col-lg-2 control-label">Data de nascimento</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										$user['inf_data_nascimento'] = substr($user['inf_data_nascimento'], 8, 2).'/'.substr($user['inf_data_nascimento'], 5, 2).'/'.substr($user['inf_data_nascimento'], 0, 4);
										echo '<label class="control-label show-info-basic">'.$user['inf_data_nascimento'].'</label>
											<input type="text" class="form-control edit-info-basic" value="'.$user['inf_data_nascimento'].'" name="inf_data_nascimento" onkeypress="mascara(this, \'##/##/####\')" maxlength="10">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputRG" class="col-lg-2 control-label">RG</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_rg'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_rg'].'" name="inf_rg">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputCPF" class="col-lg-2 control-label">CPF</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_cpf'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_cpf'].'" name="inf_cpf" readonly="">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputCTPS" class="col-lg-2 control-label">CTPS</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_ctps'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_ctps'].'" name="inf_ctps">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEndereco" class="col-lg-2 control-label">Endereço</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_endereco'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_endereco'].'" name="inf_endereco">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputCidade" class="col-lg-2 control-label">Cidade</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_cidade'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_cidade'].'" name="inf_cidade">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputUf" class="col-lg-2 control-label">Estado</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_uf'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_uf'].'" name="inf_uf" maxlength="2">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputCEP" class="col-lg-2 control-label">CEP</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_cep'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_cep'].'" name="inf_cep">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-lg-2 control-label">Email</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_email'].'</label>
										<input type="email" class="form-control edit-info-basic" value="'.$user['inf_email'].'" name="inf_email">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputTel" class="col-lg-2 control-label">Telefone</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_tel_fixo'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_tel_fixo'].'" name="inf_tel_fixo" onkeypress="mascara(this, \'## #########\')" maxlength="12">';
									}
									?>
								</div>
							</div>
							<div class="form-group">
								<label for="inputCelular" class="col-lg-2 control-label">Celular</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-basic">'.$user['inf_tel_celular'].'</label>
										<input type="text" class="form-control edit-info-basic" value="'.$user['inf_tel_celular'].'" name="inf_tel_celular" onkeypress="mascara(this, \'## #########\')" maxlength="12">';
									}
									?>
								</div>
							</div>
							<br>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<input type="button" class="btn btn-primary show-info-basic" id="editar-perfil-basic" style="float:right;" value="Editar">
									<input type="submit" class="btn btn-success edit-info-basic" id="editar-confirm-basic" style="float:right;" value="Confirmar" name="confirm-info">
									<input type="button" class="btn btn-default edit-info-basic" id="editar-cancel-basic" style="float:right;" value="Cancelar">
								</div>
							</div>
						</form>
						<hr>
						<h4>Informações bancárias</h4>
							<form class="form-horizontal" action="../controllers/EditarPerfil.php" method="POST">
							<div class="form-group">
								<label for="inputNomeBanco" class="col-lg-2 control-label">Nome do banco</label>
								<div class="col-lg-10">
									<?php 
									if(isset($user)){
										echo '<label class="control-label show-info-bank">'.$user['bnc_nome'].'</label>
										<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_nome'].'" name="bnc_nome">';
									}
									?>
								</div>
								</div>
								<div class="form-group">
									<label for="inputNumeroBanco" class="col-lg-2 control-label">Numero do banco</label>
									<div class="col-lg-10">
										<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-bank">'.$user['bnc_numero'].'</label>
											<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_numero'].'" name="bnc_numero">';
										}
										?>
									</div>
								</div>
								<div class="form-group">
									<label for="inputAgencia" class="col-lg-2 control-label">Agencia</label>
									<div class="col-lg-10">
										<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-bank">'.$user['bnc_agencia'].'</label>
											<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_agencia'].'" name="bnc_agencia">';
										}
										?>
									</div>
								</div>
								<div class="form-group">
									<label for="inputOperacao" class="col-lg-2 control-label">Operação</label>
									<div class="col-lg-10">
										<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-bank">'.$user['bnc_operacao'].'</label>
											<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_operacao'].'" name="bnc_operacao">';
										}
										?>
									</div>
								</div>
								<div class="form-group">
									<label for="inputConta" class="col-lg-2 control-label">Conta</label>
									<div class="col-lg-10">
										<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-bank">'.$user['bnc_conta'].'</label>
											<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_conta'].'" name="bnc_conta">';
										}
										?>
									</div>
								</div>
								<div class="form-group">
									<label for="inputContato" class="col-lg-2 control-label">Contato</label>
									<div class="col-lg-10">
										<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-bank">'.$user['bnc_contato'].'</label>
											<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_contato'].'" name="bnc_contato">';
										}
										?>
									</div>
								</div>
								<div class="form-group">
									<label for="inputTelBanco" class="col-lg-2 control-label">Telefone do banco</label>
									<div class="col-lg-10">
										<?php 
										if(isset($user)){
											echo '<label class="control-label show-info-bank">'.$user['bnc_telefone'].'</label>
											<input type="text" class="form-control edit-info-bank" value="'.$user['bnc_telefone'].'" name="bnc_telefone" onkeypress="mascara(this, \'## #########\')" maxlength="12">';
										}
										?>
									</div>
								</div>
							<br>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<input type="button" class="btn btn-primary show-info-bank" id="editar-perfil-bank" style="float:right;" value="Editar">
									<input type="submit" class="btn btn-success edit-info-bank" id="editar-confirm-bank" style="float:right;" value="Confirmar" name="confirm-banc">
									<input type="button" class="btn btn-default edit-info-bank" id="editar-cancel-bank" style="float:right;" value="Cancelar">
								</div>
							</div>
						</form>
						<hr>

							<form class="form-horizontal" action="../controllers/EditarPerfil.php" method="POST">
							<?php
							if(isset($user)){
								if($user['TipoFuncionario_tfn_id']=="1"){
									echo '					
									<legend>Dados de Estagiário</legend>
									<div class="form-group">
										<label for="inputCurso" class="col-lg-2 control-label">Curso</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_curso'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_curso'].'" name="est_curso">
										</div>
									</div>
									<div class="form-group">
										<label for="inputPeriodo" class="col-lg-2 control-label">Periodo atual</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_periodo'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_periodo'].'" name="est_periodo" onkeypress="mascara(this, \'####.#\')" maxlength="6">
										</div>
									</div>
									<div class="form-group">
										<label for="inputPrevisao" class="col-lg-2 control-label">Previsao de conclusão</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_prev_conclusao'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_prev_conclusao'].'" name="est_prev_conclusao" onkeypress="mascara(this, \'####.#\')" maxlength="6">
										</div>
									</div>					    
									<div class="form-group">
										<label for="select" class="col-lg-2 control-label">Termo</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_termo'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_termo'].'" name="est_termo">
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmpresa" class="col-lg-2 control-label">Empresa</label>
										<div class="col-lg-10">												
											<label class="control-label show-info-type">'.$user['est_empresa'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_empresa'].'" name="est_empresa">
										</div>
									</div>
									<div class="form-group">
										<label for="inputInicio" class="col-lg-2 control-label">Data de inicio</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_data_inicio'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_data_inicio'].'" name="est_data_inicio" onkeypress="mascara(this, \'##/##/####\')" maxlength="10">
										</div>
									</div>
									<div class="form-group">
										<label for="inputConclusao" class="col-lg-2 control-label">Data de conclusão</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_data_termino'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_data_termino'].'" name="est_data_termino" onkeypress="mascara(this, \'##/##/####\')" maxlength="10">
										</div>
									</div>
									<div class="form-group">
										<label for="inputCarga" class="col-lg-2 control-label">Carga Horária</label>
										<div class="col-lg-10">
											<label class="control-label show-info-type">'.$user['est_carga_horaria'].'</label>
											<input type="text" class="form-control edit-info-type" value="'.$user['est_carga_horaria'].'" name="est_carga_horaria">
										</div>
									</div>
									<div class="form-group show-info-type">';
										if($user['est_cond_estagio']){
											echo '<h4>Estágio Obrigatório</h4>';
										}
										else{
											echo '<h4>Estágio não obrigatório</h4>';
										}
										echo '
									</div>
									<div class="checkbox edit-info-type">
										<label>
											<input id="check_Obrigatorio" type="checkbox" name="est_cond_estagio" ';
											if($user['est_cond_estagio']){
												echo 'checked=""';
											}
											echo '> Estágio Obrigatório
										</label>
										<br>
									</div>
									<div class="form-group show-info-type">';
										if($user['est_seguro']=="option1"){
											echo '<h4>Seguro pago por: Empresa</h4>';
										}
										else{
											echo '<h4>Seguro pago por: UFF</h4>';
										}
										echo '											
									</div>
									<div class="form-group edit-info-type">
										<label class="col-lg-2 control-label">Seguro pago por</label>
										<div class="col-lg-10">
											<div class="radio">
												<label>
													<input type="radio" name="est_seguro" id="optionsRadios1" value="option1"  ';
													if($user['est_seguro']=="option1"){
														echo 'checked=""';
													}
													echo '>
													Empresa
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="est_seguro" id="optionsRadios2" value="option2"  ';
													if($user['est_seguro']=="option2"){
														echo 'checked=""';
													}
													echo '>
													UFF
												</label>
											</div>
										</div>
									</div>
									<div class="form-group show-info-type">
										<label for="inputAuxilio" class="col-lg-2 control-label">Auxílio(s):</label><br>
										<ul style="list-style-type:none;"><br><br>
											<li>';
												if($user['est_benef_bolsa']){
													echo '<label>
													Bolsa
													<div class="col-lg-10">
														<label class="control-label">'.$user['est_benef_bolsa'].'</label>
													</div>
												</label>';
											}
											echo '
											</li>
											<li>';												
												if($user['est_benef_transporte']){
													echo '<label>
													Transporte
													<div class="col-lg-10">
														<label class="control-label">'.$user['est_benef_transporte'].'</label>
													</div>
												</label>';
											}
											echo '
										</li>
										<li>';
											if($user['est_benef_refeicao']){
												echo '<label>
												Refeição
												<div class="col-lg-10">
													<label class="control-label">'.$user['est_benef_refeicao'].'</label>
												</div>
											</label>';
										}
										echo '
										</li>
										</ul>
									</div>

						<div class="form-group edit-info-type">
							<label for="inputAuxilio" class="col-lg-2 control-label">Auxílio</label><br>
							<ul style="list-style-type:none;">
								<li>
									<div class="checkbox">
										<label>
											<input id="check_Bolsa" name="check_Bolsa" type="checkbox" ';
											if($user['est_benef_bolsa']){
												echo 'checked="" ';
											}
											echo '> Bolsa
											<div class="col-lg-12">
												<input type="text" class="form-control" id="inputAuxBolsa" name="est_benef_bolsa" value="'.$user['est_benef_bolsa'].'">
											</div>
										</label>
									</div>
								</li>
								<li>
									<div class="checkbox">
										<label>
											<input id="check_Transporte" name="check_transporte" type="checkbox"';
											if($user['est_benef_transporte']){
												echo 'checked=""';
											}
											echo '> Transporte
											<div class="col-lg-12">
												<input type="text" class="form-control" id="inputAuxTransporte" name="est_benef_transporte" value="'.$user['est_benef_transporte'].'">
											</div>
										</label>
									</div>
								</li>
								<li>
									<div class="checkbox">
										<label>
											<input id="check_Refeicao" name="check_Refeicao" type="checkbox"';
											if($user['est_benef_refeicao']){
												echo 'checked=""';
											}
											echo '> Refeição
											<div class="col-lg-12">
												<input type="text" class="form-control" id="inputAuxRefeicao" name="est_benef_refeicao" value="'.$user['est_benef_refeicao'].'">
											</div>
										</label>
									</div>
								</li>

							</ul>
						</div>


						';
					}
					else{
						echo '
						<legend>Dados de Funcionário</legend>
						<div class="form-group">
							<label for="inputPIS" class="col-lg-2 control-label">PIS</label>
							<div class="col-lg-10">
								<label class="control-label show-info-type">'.$user['fnc_pis'].'</label>
								<input type="text" class="form-control edit-info-type" value="'.$user['fnc_pis'].'" name="fnc_pis">
							</div>
						</div>
						';
					}
				}

				?>

							<br>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<input type="button" class="btn btn-primary show-info-type" id="editar-perfil-type" style="float:right;" value="Editar">
									<input type="submit" class="btn btn-success edit-info-type" id="editar-confirm-type" style="float:right;" value="Confirmar" name="confirm-spec">
									<input type="button" class="btn btn-default edit-info-type" id="editar-cancel-type" style="float:right;" value="Cancelar">
								</div>
							</div>
						</form>
					</fieldset>
				</form>
			</div>
		</div>
		<?php include('../includes/footer.php'); ?>
	</div>
	<script src="../vendors/js/jquery-1.11.2.min.js"></script>
	<script src="../vendors/js/bootstrap.min.js"></script>

</body>

<script type="text/javascript">
	function mascara(t, mask){
		var i = t.value.length;
		var saida = mask.substring(1,0);
		var texto = mask.substring(i)
		if (texto.substring(0,1) != saida){
			t.value += texto.substring(0,1);
		}
	}
</script>
<script type="text/javascript">
	$(document).ready(function () {
		$("input.edit-info-user").hide();	
		$("label.edit-info-user2").hide();	
		$("input#editar-perfil-user").click(function () {
			$("label.show-info-user").hide("slow","swing");
			$("input.show-info-user").hide("slow","swing");
			$("input.edit-info-user").show("slow","swing");
			$("label.edit-info-user2").show("slow","swing");	
		});
		$("input#editar-cancel-user").click(function () {
			$("input.edit-info-user").hide("slow","swing");
			$("label.edit-info-user2").hide("slow","swing");
			$("label.show-info-user").show("slow","swing");
			$("input.show-info-user").show("slow","swing");
		});
		$("input#editar-confirm-user").click(function () {
			$("input.edit-info-user").hide("slow","swing");
			$("label.edit-info-user2").hide("slow","swing");
			$("label.show-info-user").show("slow","swing");
			$("input.show-info-user").show("slow","swing");
		});

		$("input.edit-info-basic").hide();
		$("input#editar-perfil-basic").click(function () {
			$("label.show-info-basic").hide("slow","swing");
			$("input.show-info-basic").hide("slow","swing");
			$("input.edit-info-basic").show("slow","swing");			
		});
		$("input#editar-cancel-basic").click(function () {
			$("input.edit-info-basic").hide("slow","swing");
			$("input.show-info-basic").show("slow","swing");
			$("label.show-info-basic").show("slow","swing");
		});
		$("input#editar-confirm-basic").click(function () {
			$("input.edit-info-basic").hide("slow","swing");
			$("input.show-info-basic").show("slow","swing");
			$("label.show-info-basic").show("slow","swing");
		});

		$("input.edit-info-bank").hide();
		$("input#editar-perfil-bank").click(function () {
			$("label.show-info-bank").hide("slow","swing");
			$("input.show-info-bank").hide("slow","swing");
			$("input.edit-info-bank").show("slow","swing");
		});
		$("input#editar-cancel-bank").click(function () {
			$("input.edit-info-bank").hide("slow","swing");
			$("input.show-info-bank").show("slow","swing");
			$("label.show-info-bank").show("slow","swing");
		});
		$("input#editar-confirm-bank").click(function () {
			$("input.edit-info-bank").hide("slow","swing");
			$("input.show-info-bank").show("slow","swing");
			$("label.show-info-bank").show("slow","swing");
		});

		$(".edit-info-type").hide();
		$("input#editar-perfil-type").click(function () {
			$("label.show-info-type").hide("slow","swing");
			$("input.show-info-type").hide("slow","swing");			
			$("div.show-info-type").hide("slow","swing");
			$("input.edit-info-type").show("slow","swing");	
			$("div.edit-info-type").show("slow","swing");
		});
		$("input#editar-cancel-type").click(function () {
			$(".edit-info-type").hide("slow","swing");
			$("div.show-info-type").show("slow","swing");
			$("input.show-info-type").show("slow","swing");
			$("label.show-info-type").show("slow","swing");
		});
		$("input#editar-confirm-type").click(function () {
			$(".edit-info-type").hide("slow","swing");
			$("div.show-info-type").show("slow","swing");
			$("input.show-info-type").show("slow","swing");
			$("label.show-info-type").show("slow","swing");
		});
	});
</script>
</html>