<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas</title>
</head>
<body>	
	<div class="container">
		<?php include('../includes/header_main.php'); ?>		
		<div class="form-horizontal login-inicial">
			<div class="row">
				<form action="">
					<fieldset>
						<legend>Módulos</legend>
						<table>
							<tr>
								<td><div>
									<a href="./recursosHumanos.php"class="esq btn btn-primary main_but"><br><br>Recursos Humanos</a>
								</div></td>								
								<td><div>
									<a href="./administracao.php" class="btn btn-primary main_but"><br><br>Administração</a>
								</div></td>							
							</tr>
							<tr>
								<td><div>
									<a href="./financeiro.php" class="esq btn btn-primary main_but"><br><br>Financeiro</a>
								</div></td>
								<td><div>
									<a href="./contabilidade.php" class="btn btn-primary main_but"><br><br>Contabilidade</a>
								</div></td>								
							</tr>						
						</table>


					</fieldset>	
				</form>
			</div>
		</div>
		<?php include('../includes/footer.php'); ?>
	</div>
	<script src="../vendors/js/jquery-1.11.2.min.js"></script>
	<script src="../vendors/js/bootstrap.min.js"></script>
</body>
</html>