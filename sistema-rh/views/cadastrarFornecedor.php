<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas - Cadastro</title>
</head>
<body>
	<div class="container">
		<?php include('../includes/headerRh.php'); ?>
		<div class="form-horizontal login-inicial">
			<div class="row">
				<form action="../controllers/CadastrarNovoFuncionario.php" class="form-horizontal" method="POST">
					<fieldset>
						<?php
							if(array_key_exists("sucesso", $_SESSION) && ($_SESSION["sucesso"] == true)){
		                        echo '<div class="alert alert-success" role="alert">Fornecedor cadastrado com sucesso.</div>';
		                    }
		                    unset($_SESSION['sucesso']);
						?>
						<legend>Administração - Cadastro de Forncedores</legend>
							<h4>Dados Cadastrais</h4>
								<div class="form-group">
									<label for="inputRazao" class="col-lg-2 control-label">Razão Social</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputRazao" placeholder="Razão Social" name="inf_razao">
									</div>
								</div>
								<div class="form-group">
									<label for="inputNome" class="col-lg-2 control-label">Nome Fantasia</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNome" placeholder="Nome Fantasia" name="inf_nome">
									</div>
								</div>
								<div class="form-group">
									<label for="inputInscricao" class="col-lg-2 control-label">Inscrição Estadual</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputInscricao" placeholder="Inscrição Estadual" name="inf_inscricao">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCnpj" class="col-lg-2 control-label">CNPJ</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCnpj" placeholder="00.000.000/0000-00" name="inf_cnpj">
									</div>
								</div>
								<div class="form-group">
									<label for="inputDataConstituicao" class="col-lg-2 control-label">Data de Constituição</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputDataConstituicao" placeholder="00/00/0000" name="inf_data_constituicao">
									</div>
								</div>
								<div class="form-group">
									<label for="inputAtividadePrincipal" class="col-lg-2 control-label">Atividade Econômica Principal</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputAtividadePrincipal" placeholder="Venda de bacon" name="inf_atividade_principal">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEnderecoSocial" class="col-lg-2 control-label">Endereço Social</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEnderecoSocial" placeholder="Ali, pertinho do bompreço" name="inf_endereco_social">
									</div>
								</div>
								<div class="form-group">
									<label for="inputNumeroSocial" class="col-lg-2 control-label">Número</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNumeroSocial" placeholder="1234" name="inf_numero_social">
									</div>
								</div>
								<div class="form-group">
									<label for="inputBairroSocial" class="col-lg-2 control-label">Bairro</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputBairroSocial" placeholder="Mussurunga" name="inf_bairro_social">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCidadeSocial" class="col-lg-2 control-label">Município</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCidadeSocial" placeholder="Salvador" name="inf_cidade_social">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEstadoSocial" class="col-lg-2 control-label">Estado</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEstadoSocial" placeholder="Baêa" name="inf_estado_social">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCepSocial" class="col-lg-2 control-label">CEP</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCepSocial" placeholder="41480-215" name="inf_cep_social">
									</div>
								</div>
								<div class="form-group">
									<label for="inputSite" class="col-lg-2 control-label">Site</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputSite" placeholder="www.maaxsolutions.com.br" name="inf_site">
									</div>
								</div>
								<div class="form-group">
									<label for="inputTelefone" class="col-lg-2 control-label">Telefone</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputTelefone" placeholder="(71) xxxx-xxxxx" name="inf_telefone">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCelular" class="col-lg-2 control-label">Celular</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCelular" placeholder="(71) xxxx-xxxxx" name="inf_celular">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail" class="col-lg-2 control-label">E-Mail</label>
									<div class="col-lg-10">
										<input type="email" class="form-control" id="inputEmail" placeholder="cicrano@fulano" name="inf_email">
									</div>
								</div>
								<!--div class="form-group">
									<label for="[]" class="col-lg-2 control-label">[]</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="[]" placeholder="[]" name="[]">
									</div>
								</div-->
								<!------------------------------------------------------------------------>
							<hr>
							<h4>Dados Complementares</h4>
								<div class="form-group">
									<label for="inputEnderecoEntrega" class="col-lg-2 control-label">Endereço de Entrega</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEnderecoEntrega" placeholder="Ali, pertinho do bompreço" name="inf_endereco_entrega">
									</div>
								</div>
								<div class="form-group">
									<label for="inputNumeroEntrega" class="col-lg-2 control-label">Número</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNumeroEntrega" placeholder="1234" name="inf_numero_entrega">
									</div>
								</div>
								<div class="form-group">
									<label for="inputBairroEntrega" class="col-lg-2 control-label">Bairro</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputBairroEntrega" placeholder="Mussurunga" name="inf_bairro_entrega">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCidadeEntrega" class="col-lg-2 control-label">Município</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCidadeEntrega" placeholder="Salvador" name="inf_cidade_entrega">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEstadoEntrega" class="col-lg-2 control-label">Estado</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEstadoEntrega" placeholder="Baêa" name="inf_estado_entrega">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCepEntrega" class="col-lg-2 control-label">CEP</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCepEntrega" placeholder="41480-215" name="inf_cep_entrega">
									</div>
								</div>
								<div class="form-group">
									<label for="inputComplemento" class="col-lg-2 control-label">Complemento</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputComplemento" placeholder="Perto do vale das muriçocas" name="inf_complemento">
									</div>
								</div>
								<hr>
								<div class="form-group">
									<label for="inputEnderecoEntrega2" class="col-lg-2 control-label">Endereço de Entrega</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEnderecoEntrega2" placeholder="Ali, pertinho do bompreço" name="inf_endereco_entrega2">
									</div>
								</div>
								<div class="form-group">
									<label for="inputNumeroEntrega2" class="col-lg-2 control-label">Número</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNumeroEntrega2" placeholder="1234" name="inf_numero2">
									</div>
								</div>
								<div class="form-group">
									<label for="inputBairroEntrega2" class="col-lg-2 control-label">Bairro</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputBairroEntrega2" placeholder="Mussurunga" name="inf_bairro_entrega2">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCidadeEntrega2" class="col-lg-2 control-label">Município</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCidadeEntrega2" placeholder="Salvador" name="inf_cidade_entrega2">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEstadoEntrega2" class="col-lg-2 control-label">Estado</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEstadoEntrega2" placeholder="Baêa" name="inf_estado_entrega2">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCepEntrega2" class="col-lg-2 control-label">CEP</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCepEntrega2" placeholder="41480-215" name="inf_cep_entrega2">
									</div>
								</div>
								<div class="form-group">
									<label for="inputComplemento2" class="col-lg-2 control-label">Complemento</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputComplemento2" placeholder="Perto do vale das muriçocas" name="inf_complemento2">
									</div>
								</div>
							<hr>
							<h4>Representantes e/ou Procuradores</h4>
								<div class="form-group">
									<label for="inputNomeRep" class="col-lg-2 control-label">Nome Completo</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNomeRep" placeholder="Fulano da Silva" name="inf_nome_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCpfRep" class="col-lg-2 control-label">CPF</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCpfRep" placeholder="123.321.321-12" name="inf_cpf_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputVinculoRep" class="col-lg-2 control-label">Vínculo</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputVinculoRep" placeholder="The All-owner" name="inf_vinculo_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputNacionalidadeRep" class="col-lg-2 control-label">Nacionalidade</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNacionalidadeRep" placeholder="Bronziliense" name="inf_nacionalidade_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEstadoCivilRep" class="col-lg-2 control-label">Estado Civil</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEstadoCivilRep" placeholder="Forever Alone" name="inf_estado_civil_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputDocIdentificacaoRep" class="col-lg-2 control-label">Doc. Identificação</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputDocIdentificacaoRep" placeholder="00.000.000-00" name="inf_doc_identificacao_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputDataExpedicaoRep" class="col-lg-2 control-label">Data Expedição</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputDataExpedicaoRep" placeholder="00/00/0000" name="inf_data_expedicao_rep">
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputOrgaoRep" class="col-lg-2 control-label">Orgão</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputOrgaoRep" placeholder="SSJ" name="inf_orgao_rep">
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputTelRep" class="col-lg-2 control-label">Telefone</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputTelRep" placeholder="(00)00000-0000" name="inf_tel_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCelRep" class="col-lg-2 control-label">Celular</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCelRep" placeholder="(00)00000-0000" name="inf_cel_rep">
									</div>
								</div>								
								<div class="form-group">
									<label for="inputEnderecoRep" class="col-lg-2 control-label">Endereço Social</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEnderecoRep" placeholder="Beco Diagonal" name="inf_endereco_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputNumeroRep" class="col-lg-2 control-label">Número</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputNumeroRep" placeholder="6541" name="inf_numero_rep">
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputBairroRep" class="col-lg-2 control-label">Bairro</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputBairroRep" placeholder="Mordor" name="inf_bairro_rep">
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputCidadeRep" class="col-lg-2 control-label">Município</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCidadeRep" placeholder="Valhalla" name="inf_cidade_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEstadoRep" class="col-lg-2 control-label">Estado</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputEstadoRep" placeholder="Niflheim" name="inf_estado_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputCepRep" class="col-lg-2 control-label">CEP</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputCepRep" placeholder="98.765-789" name="inf_cep_rep">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmailRep" class="col-lg-2 control-label">E-Mail</label>
									<div class="col-lg-10">
										<input type="email" class="form-control" id="inputEmailRep" placeholder="cicrano@br.com" name="inf_email_rep">
									</div>
								</div>
							<hr>
							<h4>Referências Bancárias</h4>

							<hr>
							<h4>Referências Comerciais</h4>

					</fieldset>
				</form>
			</div>
		</div>
		<?php include('../includes/footer.php'); ?>
	</div>
	<script src="../vendors/js/jquery-1.11.2.min.js"></script>
	<script src="../vendors/js/bootstrap.min.js"></script>		
	<script type="text/javascript">
		function mascara(t, mask){
			var i = t.value.length;
			var saida = mask.substring(1,0);
			var texto = mask.substring(i)
			if (texto.substring(0,1) != saida){
				t.value += texto.substring(0,1);
			}
		}
	 </script>
</body>	
</html>
