<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="logo">
				<img class="logo" src="../assets/images/logo_MAAX.png" alt="MAAX Solutions">
			</div>
		</div>

		<div class="form-horizontal login-inicial">
			<div class="row">            

				<?php
                    session_start();
					if(array_key_exists("logged", $_SESSION) && ($_SESSION["logged"] == false)){
                        echo '<div class="alert alert-danger" role="alert">Usuário ou senha incorretos.</div>';
                    }
				?>

			<form action="../controllers/ValidarLogin.php" method="POST">
				<fieldset>
					<legend>RH Salinas</legend>
					<div class="form-group">
						<label for="inputLogin" class="col-lg-2 control-label">Login</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputLogin" placeholder="Login do usuário" name="login">
						</div>
					</div>

					<div class="form-group">
						<label for="inputPassword" class="col-lg-2 control-label">Senha</label>
						<div class="col-lg-10">
							<input type="password" class="form-control" id="inputPassword" placeholder="Senha" name="senha">
						</div>
					</div>

					<a href="" style="font-size: 16px"runat="server" data-target="#modal_esqueci" data-toggle="modal" id="link_esqueci">Esqueci minha senha</a>
					<button style="float: right;position: relative" type="submit" class="btn btn-primary">Enviar</button>

				</fieldset>	
			</form>
		</div>
	</div>
	<?php include('../includes/footer.php'); ?>
</div>
<script src="../vendors/js/jquery-1.11.2.min.js"></script>
<script src="../vendors/js/bootstrap.min.js"></script>
</body>

	<div class="modal fade" id="modal_esqueci" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <?php 
                    if(isset($_SESSION['enviado'])||isset($_SESSION['nenviado'])){
                        echo '<script type="text/javascript">
                        $(document).ready(function(){
                            $("#modal_esqueci").modal("show");                            
                        });
                        </script>'; 
                    }                                    
                ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Esqueci minha senha
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <form action="../controllers/EsqueciSenha.php" method="POST">
                            <div class="form-group">
                                <label for="inputEmail1" style="text-align:left;" class="col-lg-10 control-label">
                                Digite o e-mail cadastrado:
                                </label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" id="inputEmail1" placeholder="Email" name="email">
                                </div>
                            </div>                        
                            <div class="modal-footer">
                                <?php 
                                    if(isset($_SESSION['nenviado'])){
                                        echo '
                                            <div class="alert alert-dismissable alert-danger emailInvalido"> 
                                                <button type="button" class="close" data-dismiss="alert">×</button>                         
                                                E-mail não encontrado:
                                                <strong>
                                                    '.$_SESSION['nenviado'].'
                                                </strong>                                
                                            </div>'; 
                                        unset($_SESSION['nenviado']);
                                    }    
                                    if(isset($_SESSION['enviado'])){
                                        echo '
                                            <div class="alert alert-dismissable alert-success emailEnviado">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                Um e-mail com sua senha foi enviado para:
                                                <strong>
                                                    '.$_SESSION['enviado'].'
                                                </strong>
                                            </div>'; 
                                        unset($_SESSION['enviado']);
                                    }                                  
                                ?>                                
                                
                                <button type="submit" class="btn btn-primary" onclick="getEmail()">Enviar</button><!--adicionar click acesso ao banco-->
                                <!--<a data-target="#modal_esqueci_false" data-toggle="modal" class="btn btn-primary" onclick="getEmail()">Enviar</a>-->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- form -->
            </div>
        </div>        
                
        <script type="text/javascript">
        	function getEmail() { 
        		var esqueciEmail = document.getElementById("inputEmail1");
        		var output= document.getElementById("textoemail");
        		var output2= document.getElementById("textoemail2");
        		output.innerHTML= esqueciEmail.value;
        		output2.innerHTML= esqueciEmail.value;
        	}      		
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("div#emailEnviado").hide(); 
                $("div#emailInvalido").hide();               
            });
        </script>

</html>