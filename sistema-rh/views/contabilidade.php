<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas - Contabilidade</title>
</head>
<body>
	<div class="container">
		<?php include('../includes/headerContabilidade.php'); ?>
		<div class="form-horizontal login-inicial">
			<div class="row">
				<form class="form-horizontal" method="POST">
					<fieldset>
						<legend>Contabilidade</legend>
						<div class="center-block">
						<h4>Lançamento de NF</h4>	
                            <div class="jumbotron col-lg-6 col-lg-offset-3" style="margin-right:100px;">
                            <img src="../assets/images/logo_MAAX.png" style="width:136px;height:136px;" alt="">
							<a href="#" class="btn btn-primary btn-lg btn-block">Emitir</a></div></div>
						<div class="center-block">
						<h4>Emissão de relatórios</h4>
							<div class="jumbotron col-lg-6 col-lg-offset-3" style="margin-right:100px;">
                            <img src="../assets/images/logo_MAAX.png" style="width:136px;height:136px;" alt="">
							<a href="#" class="btn btn-primary btn-lg btn-block">Emitir</a></div></div>
					</fieldset>
				</form>
			</div>
		</div>
		<?php include('../includes/footer.php'); ?>
	</div>
	<script src="../vendors/js/jquery-1.11.2.min.js"></script>
	<script src="../vendors/js/bootstrap.min.js"></script>
</body>
</html>