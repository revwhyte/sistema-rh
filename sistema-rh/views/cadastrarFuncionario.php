<?php 
session_start();
if(!isset($_SESSION["logged"]))
{
    header('Location:../views/home.php');
} 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link  type="text/css" rel="stylesheet" href="../vendors/css/bootstrap.min.css">
	<script src="../vendors/js/bootstrap.min.js"></script>  
	<link rel="stylesheet" href="../assets/css/home.css">
	<title>RH Salinas - Cadastro</title>
</head>
<body>
	<div class="container">
		<?php include('../includes/headerRh.php'); ?>
		<div class="form-horizontal login-inicial">
			<div class="row">
				<form action="../controllers/CadastrarNovoFuncionario.php" class="form-horizontal" method="POST">
					<fieldset>
					<?php
						if(array_key_exists("sucesso", $_SESSION) && ($_SESSION["sucesso"] == true)){
	                        echo '<div class="alert alert-success" role="alert">Funcionário cadastrado com sucesso.</div>';
	                    }
	                    unset($_SESSION['sucesso']);
					?>
					<?php
						if(array_key_exists("cpf_usado", $_SESSION) && ($_SESSION["cpf_usado"] == true)){
	                        echo '<div class="alert alert-danger" role="alert">CPF já utilizado.</div>';
	                    }
	                    unset($_SESSION['cpf_usado']);
					?>
					<?php
						if(array_key_exists("conta_usada", $_SESSION) && ($_SESSION["conta_usada"] == true)){
	                        echo '<div class="alert alert-danger" role="alert">Conta bancária já utilizada.</div>';
	                    }
	                    unset($_SESSION['conta_usada']);
					?>
						<legend>RH Salinas - Cadastro</legend>
						<div class="checkbox">
							<label>
								<input id="check_estag" type="checkbox" onchange="trocaForm()" name="check_estag"> Estagiário
							</label>
						</div>
						<br>
					<div class="form-group">
						<label for="inputName" class="col-lg-2 control-label">Nome completo</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputName" placeholder="Nome Completo" name="inf_nome">
						</div>
					</div>
					<div class="form-group">
						<label for="inputDate" class="col-lg-2 control-label">Data de nascimento</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputDate" placeholder="DD/MM/AAAA" name="inf_data_nascimento" onkeypress="mascara(this, '##/##/####')" maxlength="10">
						</div>
					</div>
					<div class="form-group">
						<label for="inputRG" class="col-lg-2 control-label">RG</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputRG" placeholder="RG" name="inf_rg">
						</div>
					</div>
					<div class="form-group">
						<label for="inputCPF" class="col-lg-2 control-label">CPF</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputCPF" placeholder="CPF" name="inf_cpf" onkeypress="mascara(this, '###.###.###-##')" maxlength="14">
						</div>
					</div>
					<div class="form-group">
						<label for="inputCTPS" class="col-lg-2 control-label">CTPS</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputCTPS" placeholder="CTPS" name="inf_ctps">
						</div>
					</div>
					<div class="form-group">
						<label for="inputEndereco" class="col-lg-2 control-label">Endereço</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputEndereço" placeholder="Endereço" name="inf_endereco">
						</div>
					</div>
					<div class="form-group">
						<label for="inputCidade" class="col-lg-2 control-label">Cidade</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputCidade" placeholder="Cidade" name="inf_cidade">
						</div>
					</div>
					<div class="form-group">
						<label for="inputUf" class="col-lg-2 control-label">Estado</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputUf" placeholder="UF" name="inf_uf" maxlength="2">
						</div>
					</div>
					<div class="form-group">
						<label for="inputCEP" class="col-lg-2 control-label">CEP</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputCEP" placeholder="CEP" name="inf_cep" onkeypress="mascara(this, '##.###-###')" maxlength="10">
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-lg-2 control-label">Email</label>
						<div class="col-lg-10">
							<input type="email" class="form-control" id="inputEmail" placeholder="Email" name="inf_email">
						</div>
					</div>
					<div class="form-group">
						<label for="inputTel" class="col-lg-2 control-label">Telefone</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputTel" placeholder="(xx) x xxxx-xxxx" name="inf_tel_fixo" onkeypress="mascara(this, '## #########')" maxlength="13">
						</div>
					</div>
					<div class="form-group">
						<label for="inputCelular" class="col-lg-2 control-label">Celular</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputCelular" placeholder="(xx) x xxxx-xxxx" name="inf_tel_celular" onkeypress="mascara(this, '## #########')" maxlength="13">
						</div>
					</div>
					<legend>Dados Bancários</legend>
					<div class="form-group">
						<label for="inputNomeBanco" class="col-lg-2 control-label">Nome do banco</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputNomeBanco" placeholder="Ex: banco da Maax" name="bnc_nome">
						</div>
					</div>
					<div class="form-group">
						<label for="inputNumeroBanco" class="col-lg-2 control-label">Numero do banco</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputNumerBanco" placeholder="?????" name="bnc_numero">
						</div>
					</div>
					
					<div class="form-group">
						<label for="inputAgencia" class="col-lg-2 control-label">Agencia</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputAgencia" placeholder="xxxx-x" name="bnc_agencia">
						</div>
					</div>
					<div class="form-group">
						<label for="inputOperacao" class="col-lg-2 control-label">Operação</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputoperacao" placeholder="xxx" name="bnc_operacao" maxlength="3">
						</div>
					</div>
					<div class="form-group">
						<label for="inputConta" class="col-lg-2 control-label">Conta</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputConta" placeholder="xxxx-x" name="bnc_conta">
						</div>
					</div>
					<div class="form-group">
						<label for="inputContato" class="col-lg-2 control-label">Contato</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputContato" placeholder="Nome do contato" name="bnc_contato">
						</div>
					</div>
					<div class="form-group">
						<label for="inputTelBanco" class="col-lg-2 control-label">Telefone do banco</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="inputTelBanco" placeholder="xx x xxxx-xxxx" name="bnc_telefone" onkeypress="mascara(this, '## #########')" maxlength="13">
						</div>
					</div>
					<br>


					<!---------------------------------------------------------------->

					<div id="func"><?php include('../includes/form_func.php'); ?></div>
					<div id="estag"><?php include('../includes/form_estag.php'); ?></div>

					<!---------------------------------------------------------------->
					<br>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<input data-target="#modal_cadastro1" data-toggle="modal" type="submit" class="btn btn-primary" style="float:right;" placeholder="Enviar">
						</div>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
			<?php include('../includes/footer.php'); ?>
		</div>
		<script src="../vendors/js/jquery-1.11.2.min.js"></script>
		<script src="../vendors/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function () {
				$("div#estag").hide();
				$("input#check_estag").click(function () {
					$("div#estag").toggle();
					$("div#func").toggle();
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function () {
				$("input#inputAuxTransporte").hide();
				$("input#check_Transporte").click(function () {
					$("input#inputAuxTransporte").toggle();
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function () {
				$("input#inputAuxRefeicao").hide();
				$("input#check_Refeicao").click(function () {
					$("input#inputAuxRefeicao").toggle();
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function () {
				$("input#inputAuxBolsa").hide();
				$("input#check_Bolsa").click(function () {
					$("input#inputAuxBolsa").toggle();
				});
			});
		</script>
		<script type="text/javascript">
			function mascara(t, mask){
				var i = t.value.length;
				var saida = mask.substring(1,0);
				var texto = mask.substring(i)
				if (texto.substring(0,1) != saida){
					t.value += texto.substring(0,1);
				}
			}
		 </script>
	

	</body>	
</html>