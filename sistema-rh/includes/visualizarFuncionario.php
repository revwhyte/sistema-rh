<div class="form-horizontal login-inicial">
	<div class="row">
		<form action="../controllers/EditarFuncionario.php" class="form-horizontal" method="POST">
			<fieldset>
				<legend>Visualizar Funcionário</legend>
				<?php 
					require_once('../models/Database.php');
					require_once('../models/Funcionario.php');
					require_once('../models/TipoFuncionario.php');
					require_once('../models/DadosBancarios.php');
					require_once('../models/DadosBancoFuncionario.php');
					require_once('../models/AcessoFuncionario.php');

					if(isset($_SESSION) && isset($_SESSION['lista_func']) && isset($i))
					{
						$user = $_SESSION['lista_func'][$i];
						
						$conn = Database::openConn();
						$banco_func = DadosBancoFuncionario::readDadosBancoFuncionario($user['inf_id'], $conn);
						$dados_banc = DadosBancarios::readDadosBancarios($banco_func[0]['DadosBancarios_bnc_id'], $conn);

						$acesso_func = AcessoFuncionario::readAcessoFuncionario($user['inf_id'],$conn);
						if($acesso_func){
							$modal = 2;
						}
						else{
							$modal = 1;
						}
						
						$conn = Database::closeConn();	
						$user = array_merge($user, $dados_banc[0]);				
					}
				?>
				<div class="checkbox">
					<?php
					if(isset($user)){
						if($user['TipoFuncionario_tfn_id']=="1"){
							echo '<h2>Estagiário</h2>';
						}
						else{
							if($user['TipoFuncionario_tfn_id']=="2"){
								echo '<h2>Funcionário</h2>';
							}
							else{
								echo '<h2>Prestador de serviço</h2>';
							}
						}
					}

					?>
				</div>
				<br>
				<div class="form-group">
					<label for="inputName" class="col-lg-2 control-label">Nome completo</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_nome'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_nome'].'" name="inf_nome">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputDate" class="col-lg-2 control-label">Data de nascimento</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							$user['inf_data_nascimento'] = substr($user['inf_data_nascimento'], 8, 2).'/'.substr($user['inf_data_nascimento'], 5, 2).'/'.substr($user['inf_data_nascimento'], 0, 4);
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_data_nascimento'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_data_nascimento'].'" name="inf_data_nascimento" onkeypress="mascara(this, \'##/##/####\')" maxlength="10">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputRG" class="col-lg-2 control-label">RG</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_rg'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_rg'].'" name="inf_rg">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCPF" class="col-lg-2 control-label">CPF</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_cpf'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_cpf'].'" name="inf_cpf" readonly="">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCTPS" class="col-lg-2 control-label">CTPS</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_ctps'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_ctps'].'" name="inf_ctps">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEndereco" class="col-lg-2 control-label">Endereço</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_endereco'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_endereco'].'" name="inf_endereco">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCidade" class="col-lg-2 control-label">Cidade</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_cidade'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_cidade'].'" name="inf_cidade">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputUf" class="col-lg-2 control-label">Estado</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_uf'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_uf'].'" name="inf_uf" maxlength="2">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCEP" class="col-lg-2 control-label">CEP</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_cep'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_cep'].'" name="inf_cep">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Email</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_email'].'</label>
							<input type="email" class="form-control label-edit'.$i.'" value="'.$user['inf_email'].'" name="inf_email">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputTel" class="col-lg-2 control-label">Telefone</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_tel_fixo'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_tel_fixo'].'" name="inf_tel_fixo" onkeypress="mascara(this, \'## #########\')" maxlength="12">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCelular" class="col-lg-2 control-label">Celular</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['inf_tel_celular'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['inf_tel_celular'].'" name="inf_tel_celular" onkeypress="mascara(this, \'## #########\')" maxlength="12">';
						}
						?>
					</div>
				</div>
				<legend>Dados Bancários</legend>
				<div class="form-group">
					<label for="inputNomeBanco" class="col-lg-2 control-label">Nome do banco</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_nome'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_nome'].'" name="bnc_nome">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputNumeroBanco" class="col-lg-2 control-label">Numero do banco</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_numero'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_numero'].'" name="bnc_numero">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputAgencia" class="col-lg-2 control-label">Agencia</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_agencia'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_agencia'].'" name="bnc_agencia">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputOperacao" class="col-lg-2 control-label">Operação</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_operacao'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_operacao'].'" name="bnc_operacao">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputConta" class="col-lg-2 control-label">Conta</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_conta'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_conta'].'" name="bnc_conta">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputContato" class="col-lg-2 control-label">Contato</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_contato'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_contato'].'" name="bnc_contato">';
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputTelBanco" class="col-lg-2 control-label">Telefone do banco</label>
					<div class="col-lg-10">
						<?php 
						if(isset($user)){
							echo '<label class="control-label label-read'.$i.'">'.$user['bnc_telefone'].'</label>
							<input type="text" class="form-control label-edit'.$i.'" value="'.$user['bnc_telefone'].'" name="bnc_telefone" onkeypress="mascara(this, \'## #########\')" maxlength="12">';
						}
						?>
					</div>
				</div>
				<!---------------------------------------------------------------->
				<?php
				if(isset($user)){
					if($user['TipoFuncionario_tfn_id']=="1"){
						echo '					
						<legend>Dados de Estagiário</legend>
						<div class="form-group">
							<label for="inputCurso" class="col-lg-2 control-label">Curso</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_curso'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_curso'].'" name="est_curso">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPeriodo" class="col-lg-2 control-label">Periodo atual</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_periodo'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_periodo'].'" name="est_periodo" onkeypress="mascara(this, \'####.#\')" maxlength="6">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPrevisao" class="col-lg-2 control-label">Previsao de conclusão</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_prev_conclusao'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_prev_conclusao'].'" name="est_prev_conclusao" onkeypress="mascara(this, \'####.#\')" maxlength="6">
							</div>
						</div>					    
						<div class="form-group">
							<label for="select" class="col-lg-2 control-label">Termo</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_termo'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_termo'].'" name="est_termo">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmpresa" class="col-lg-2 control-label">Empresa</label>
							<div class="col-lg-10">												
								<label class="control-label label-read'.$i.'">'.$user['est_empresa'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_empresa'].'" name="est_empresa">
							</div>
						</div>
						<div class="form-group">
							<label for="inputInicio" class="col-lg-2 control-label">Data de inicio</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_data_inicio'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_data_inicio'].'" name="est_data_inicio" onkeypress="mascara(this, \'##/##/####\')" maxlength="10">
							</div>
						</div>
						<div class="form-group">
							<label for="inputConclusao" class="col-lg-2 control-label">Data de conclusão</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_data_termino'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_data_termino'].'" name="est_data_termino" onkeypress="mascara(this, \'##/##/####\')" maxlength="10">
							</div>
						</div>
						<div class="form-group">
							<label for="inputCarga" class="col-lg-2 control-label">Carga Horária</label>
							<div class="col-lg-10">
								<label class="control-label label-read'.$i.'">'.$user['est_carga_horaria'].'</label>
								<input type="text" class="form-control label-edit'.$i.'" value="'.$user['est_carga_horaria'].'" name="est_carga_horaria">
							</div>
						</div>
						<div class="form-group label-read'.$i.'">';
							if($user['est_cond_estagio']){
								echo '<h4>Estágio Obrigatório</h4>';
							}
							else{
								echo '<h4>Estágio não obrigatório</h4>';
							}
							echo '
						</div>
						<div class="checkbox label-edit'.$i.'">
							<label>
								<input id="check_Obrigatorio" type="checkbox" name="est_cond_estagio" ';
									if($user['est_cond_estagio']){
										echo 'checked=""';
									}
									echo '> Estágio Obrigatório
							</label>
							<br>
						</div>
						<div class="form-group label-read'.$i.'">';
							if($user['est_seguro']=="option1"){
								echo '<h4>Seguro pago por: Empresa</h4>';
							}
							else{
								echo '<h4>Seguro pago por: UFF</h4>';
							}
							echo '											
						</div>
						<div class="form-group label-edit'.$i.'">
							<label class="col-lg-2 control-label">Seguro pago por</label>
							<div class="col-lg-10">
								<div class="radio">
									<label>
										<input type="radio" name="est_seguro" id="optionsRadios1" value="option1"  ';
										if($user['est_seguro']=="option1"){
											echo 'checked=""';
										}
										echo '>
										Empresa
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="est_seguro" id="optionsRadios2" value="option2"  ';
										if($user['est_seguro']=="option2"){
											echo 'checked=""';
										}
										echo '>
										UFF
									</label>
								</div>
							</div>
						</div>
						<div class="form-group label-read'.$i.'">
							<label for="inputAuxilio" class="col-lg-2 control-label">Auxílio(s):</label><br>
							<ul style="list-style-type:none;"><br><br>
								<li>';
									if($user['est_benef_bolsa']){
										echo '<label>
										Bolsa
										<div class="col-lg-10">
											<label class="control-label">'.$user['est_benef_bolsa'].'</label>
										</div>
									</label>';
									}
									echo '
								</li>
								<li>';												
									if($user['est_benef_transporte']){
										echo '<label>
										Transporte
										<div class="col-lg-10">
											<label class="control-label">'.$user['est_benef_transporte'].'</label>
										</div>
									</label>';
									}
									echo '
								</li>
								<li>';
									if($user['est_benef_refeicao']){
										echo '<label>
										Refeição
										<div class="col-lg-10">
											<label class="control-label">'.'R$ '.(float)$user['est_benef_refeicao'].'</label>
										</div>
									</label>';
								}
								echo '
								</li>
							</ul>
						</div>

						<div class="form-group label-edit'.$i.'">
						<label for="inputAuxilio" class="col-lg-2 control-label">Auxílio</label><br>
						<ul style="list-style-type:none;">
							<li>
								<div class="checkbox">
									<label>
										<input id="check_Bolsa" name="check_Bolsa" type="checkbox" ';
											if($user['est_benef_bolsa']){
												echo 'checked="" ';
											}
											echo '> Bolsa
										<div class="col-lg-12">
											<input type="text" class="form-control" id="inputAuxBolsa" name="est_benef_bolsa" value="'.$user['est_benef_bolsa'].'">
										</div>
									</label>
								</div>
							</li>
							<li>
								<div class="checkbox">
									<label>
										<input id="check_Transporte" name="check_transporte" type="checkbox"';
											if($user['est_benef_transporte']){
												echo 'checked=""';
											}
											echo '> Transporte
										<div class="col-lg-12">
											<input type="text" class="form-control" id="inputAuxTransporte" name="est_benef_transporte" value="'.$user['est_benef_transporte'].'">
										</div>
									</label>
								</div>
							</li>
							<li>
								<div class="checkbox">
									<label>
										<input id="check_Refeicao" name="check_Refeicao" type="checkbox"';
											if($user['est_benef_refeicao']){
												echo 'checked=""';
											}
											echo '> Refeição
										<div class="col-lg-12">
											<input type="text" class="form-control" id="inputAuxRefeicao" name="est_benef_refeicao" value="'.$user['est_benef_refeicao'].'">
										</div>
									</label>
								</div>
							</li>
							
						</ul>
					</div>


			';
		}
		else{
			echo '
			<legend>Dados de Funcionário</legend>
			<div class="form-group">
				<label for="inputPIS" class="col-lg-2 control-label">PIS</label>
				<div class="col-lg-10">
					<label class="control-label label-read'.$i.'">'.$user['fnc_pis'].'</label>
					<input type="text" class="form-control label-edit'.$i.'" value="'.$user['fnc_pis'].'" name="fnc_pis">
				</div>
			</div>
			';
		}
	}

	?>
	<br>

	<div class="modal-footer">
		<?php  echo '
		<button type="button" id="btnRmvrUsuario-'.$i.'" class="btn btn-danger btnUser label-read'.$i.'">Remover</button>
		<button type="button" id="btnEditUsuario-'.$i.'" class="btn btn-primary btnUser label-read'.$i.'">Editar</button>
		<button type="button" id="btnCriaUsuario-'.$i.'" data-toggle="modal" class="btn btn-success btnUser label-read'.$i.'">Usuário</button>

		
		<button type="button" id="btnCancUsuario-'.$i.'" class="btn btn-danger btnUser label-edit'.$i.'">Cancelar</button>
		<button type="submit" value="'.$user['inf_id'].'" name ="inf_id" id="btnConfUsuario-'.$i.'" class="btn btn-success btnUser label-edit'.$i.'">Confirmar</button>';
		?>
	</div>
</fieldset>
</form>
</div>
</div>

<?php echo'
<div class="modal fade" id="modal_cadastro_usuario'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">';
      		if($modal==1){
              echo '
		          <div class="modal-header">
		              <button type="button" class="close" data-dismiss="modal">
		                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		              </button>
		              <h4 class="modal-title" id="myModalLabel">
		                  Criar conta de usuário:
		              </h4>
		          </div>
		          <div class="modal-body">
		              <div class="form-horizontal">
		              	<form action="../controllers/CadastrarNovoUsuario.php" method="POST">
		                  <div class="form-group">
		                    <div class="form-group">
			                    <p for="criar_user" style="text-align:center;" class="control-label">
			                    Deseja criar uma conta de usuário<br> do sistema para este funcionário?
			                    </p>
			                </div>
			                <div class="form-group">
			                    <label for="inputMatricula" class="col-lg-4 control-label">Matrícula</label>
			                    <div class="col-lg-12">
			                	    <input type="text" class="form-control" id="inputMatricula" value="'.$user['inf_matricula'].'" name="matricula" readonly="">
			                    </div>
			                </div>
			                <div class="form-group">
						      <label for="select" class="col-lg-2 control-label">Nivel de Acesso</label>
						      <div class="col-lg-10">
						        <select class="form-control" id="select" name="acf_nivel_acesso">
						          <option>1</option>
						          <option>2</option>
						          <option>3</option>
						          <option>4</option>
						          <option>5</option>
						        </select>
						      </div>
						    </div>
		                  </div>                        
		                  <div class="modal-footer">                  
			                  <button id="btn_cancelar_criar" type="reset" data-dismiss="modal" name="btn_cancelar" class="btn btn-danger">
			                      Não
			                  </button>
			                  <button id="btn_cadastrar_criar" type="submit" runat="server" name="btn_cadastrar" class="btn btn-success">
			                      Sim
			                  </button>                  
		              	  </div>
		              	 </form>
		              </div>
		          </div>
		          <!-- form -->';
      		}
      		else{
      			echo '
		          <div class="modal-header">
		              <button type="button" class="close" data-dismiss="modal">
		                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		              </button>
		              <h4 class="modal-title" id="myModalLabel">
		                  Deletar conta de usuário:
		              </h4>
		          </div>
		          <div class="modal-body">
		              <div class="form-horizontal">
		              	<form action="../controllers/DeletarUsuario.php" method="POST">
		                  <div class="form-group">
		                    <div class="form-group">
			                    <p for="criar_user" style="text-align:center;" class="control-label">
			                    Deseja deletar a conta de usuário<br> do sistema para este funcionário?
			                    </p>
			                </div>
			                <div class="form-group">
			                    <label for="inputMatricula" class="col-lg-4 control-label">Matrícula</label>
			                    <div class="col-lg-12">
			                	    <input type="text" class="form-control" id="inputMatricula" value="'.$user['inf_matricula'].'" name="matricula" readonly="">
			                    </div>
			                </div>	                
		                  </div>                        
		                  <div class="modal-footer">                  
			                  <button id="btn_cancelar_criar" type="reset" data-dismiss="modal" name="btn_cancelar" class="btn btn-danger">
			                      Não
			                  </button>
			                  <button id="btn_cadastrar_criar" type="submit" value="'.$user['inf_id'].' runat="server" name="btn_deletar_user" class="btn btn-success">
			                      Sim
			                  </button>                  
		              	  </div>
		              	 </form>
		              </div>
		          </div>
		          <!-- form -->';
      		}
      	echo '
      </div>
  </div>
</div>

<div class="modal fade" id="modal_confirmar_exclusao'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">
                  Excluir Funcionário
              </h4>
          </div>
          <div class="modal-body">
              <div class="form-horizontal">
                  <div class="form-group">
                    <div class="form-group">
                        <p for="criar_user" style="text-align:center;" class="control-label">
                        Deseja realmente excluir este funcionário?
                        </p>
                    </div>                    
                  </div>                        
                  <div class="modal-footer">
                  <form action="../controllers/DeletarFuncionario.php" method="POST">
	                  <button id="btn_cancelar_confirm" type="reset" data-dismiss="modal" name="btn_cancelar" class="btn btn-danger">
	                      Não
	                  </button>                  
	                  <button id="btn_deletar_confirm" type="submit" value="'.$user['inf_id'].'" runat="server" name="btn_deletar_confirm" class="btn btn-success">
	                      Sim
	                  </button>
                  </form>
                  </div>
              </div>
          </div>
          <!-- form -->
      </div>
  </div>
</div>

'; ?>