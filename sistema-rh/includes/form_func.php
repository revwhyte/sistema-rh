<!--Se funcionario-->
<legend>Dados de Funcionário</legend>	
    <div class="form-group">
	<label class="col-lg-2 control-label">Tipo do funcionário</label>
	<div class="col-lg-10">
		<div class="radio">
			<label>
				<input type="radio" name="tipo_func" id="optionsRadios1" value="option1"  checked="">
				Funcionário
			</label>
		</div>
		<div class="radio">
			<label>
				<input type="radio" name="tipo_func" id="optionsRadios2" value="option2">
				Prestador de serviços
			</label>
		</div>
	</div><br><br><br><br>
	<div class="form-group">
      <label for="inputPIS" class="col-lg-2 control-label">PIS</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputPIS" placeholder="PIS" name="fnc_pis">
      </div>
    </div>
</div>			