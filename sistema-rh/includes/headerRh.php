<div class="row">
  <div class="logo">
    <img class="logo" src="../assets/images/logo_MAAX.png" target="_blank" alt="MAAX Solutions">
  </div>
</div>
<div class="navbar navbar-inverse">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="http://maaxsolutions.com.br" target="_blank">Maax Solutions</a>
  </div>
  <div class="navbar-collapse collapse navbar-inverse-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="../views/main.php">Principal</a></li>      
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ações <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li class="dropdown-header">Funcionário</li>
          <li><a href="../views/cadastrarFuncionario.php">Cadastrar</a></li>
          <li><a href="../views/consultarFuncionario.php">Consultar</a></li>
          <li class="divider"></li>
          <li class="dropdown-header">Usuário</li>
          <li><a href="../views/consultarUsuario.php">Consultar</a></li>
          
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a disabled="" class="disabled">Recursos Humanos</a></li>
      <li><a href="../views/perfil.php"><span class="glyphicon glyphicon-user"></span> Perfil</a></li>
      <li><a href="../controllers/Logout.php" type="submit" id="sair"><span class="glyphicon glyphicon-off"></span> Sair</a></li>
    </ul>
  </div>
</div>