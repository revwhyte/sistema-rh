<!--Se estagiario-->
<legend>Dados de Estagiário</legend>
<div class="form-group">
	<label for="inputCurso" class="col-lg-2 control-label">Curso</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputCurso" placeholder="Curso" name="est_curso">
	</div>
</div>
<div class="form-group">
	<label for="inputPeriodo" class="col-lg-2 control-label">Periodo atual</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputPeriodo" placeholder="ex: 2015.1" name="est_periodo" onkeypress="mascara(this, '####.#')" maxlength="6">
	</div>
</div>
<div class="form-group">
	<label for="inputPrevisao" class="col-lg-2 control-label">Previsao de conclusão</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputPrevisao" placeholder="ex: 2020.2" name="est_prev_conclusao" onkeypress="mascara(this, '####.1')" maxlength="6">
	</div>
</div>					    
<div class="form-group">
	<label for="select" class="col-lg-2 control-label">Termo</label>
	<div class="col-lg-10">
		<select class="form-control" id="select" name="est_termo">
			<option>Termo de compromisso</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="inputEmpresa" class="col-lg-2 control-label">Empresa</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputEmpresa" value="Maax Solutions" name="est_empresa" readonly="">
	</div>
</div>
<div class="form-group">
	<label for="inputInicio" class="col-lg-2 control-label">Data de inicio</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputInicio" placeholder="DD/MM/AAAA" name="est_data_inicio" onkeypress="mascara(this, '##/##/####')" maxlength="10">
	</div>
</div>
<div class="form-group">
	<label for="inputConclusao" class="col-lg-2 control-label">Data de conclusão</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputConclusao" placeholder="DD/MM/AAAA" name="est_data_termino" onkeypress="mascara(this, '##/##/####')" maxlength="10">
	</div>
</div>
<div class="form-group">
	<label for="inputCarga" class="col-lg-2 control-label">Carga Horária</label>
	<div class="col-lg-10">
		<input type="text" class="form-control" id="inputCarga" placeholder="ex: 20h" name="est_carga_horaria">
	</div>
</div>
<div class="checkbox">
	<label>
		<input id="check_Obrigatorio" type="checkbox" name="est_cond_estagio"> Estágio Obrigatório
	</label>
	<br>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Seguro pago por</label>
	<div class="col-lg-10">
		<div class="radio">
			<label>
				<input type="radio" name="est_seguro" id="optionsRadios1" value="option1"  checked="">
				Empresa
			</label>
		</div>
		<div class="radio">
			<label>
				<input type="radio" name="est_seguro" id="optionsRadios2" value="option2">
				UFF
			</label>
		</div>
	</div>
</div>

<div class="form-group">
	<label for="inputAuxilio" class="col-lg-2 control-label">Auxílio</label><br>
	<ul style="list-style-type:none;">
		<li>
			<div class="checkbox">
				<label>
					<input id="check_Bolsa" name="check_Bolsa" type="checkbox"> Bolsa
					<div class="col-lg-12">
						<input type="text" class="form-control" id="inputAuxBolsa" name="est_benef_bolsa" placeholder="R$ 00,00">
					</div>
				</label>
			</div>
		</li>
		<li>
			<div class="checkbox">
				<label>
					<input id="check_Transporte" name="check_transporte" type="checkbox"> Transporte
					<div class="col-lg-12">
						<input type="text" class="form-control" id="inputAuxTransporte" name="est_benef_transporte" placeholder="R$ 00,00">
					</div>
				</label>
			</div>
		</li>
		<li>
			<div class="checkbox">
				<label>
					<input id="check_Refeicao" name="check_Refeicao" type="checkbox"> Refeição
					<div class="col-lg-12">
						<input type="text" class="form-control" id="inputAuxRefeicao" name="est_benef_refeicao" placeholder="R$ 00,00">
					</div>
				</label>
			</div>
		</li>
		
	</ul>
</div>