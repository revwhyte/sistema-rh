<?php
	require_once('../models/Database.php');
	require_once('../models/Funcionario.php');
	require_once('../models/TipoFuncionario.php');
	require_once('../models/DadosBancarios.php');
	require_once('../models/DadosBancoFuncionario.php');
	require_once('../models/AcessoFuncionario.php');

	session_start();
	$user = $_SESSION['profile'][0][0];
	$profile = $_SESSION['profile'][0];

	if(isset($_POST['confirm-user'])){
		if($_POST['nova_senha']!=$_POST['confirmar_senha']){
			$_SESSION['senha_diferente'] = true;
			header('Location: ../views/perfil.php');
			die();
		}
		$conn = Database::openConn();
		$acesso_func = AcessoFuncionario::readAcessoFuncionario($profile['inf_id'],$conn)[0];
		if(md5($_POST['senha_confirmar_senha'])==$user['acf_senha']){
			$user['acf_senha'] = md5($_POST['nova_senha']); 

			$newuser = new AcessoFuncionario($user);
			$newuser->updateAcessoFuncionario($user['acf_id'], $conn);

			$_SESSION['profile'][0][0] = $user;
			$_SESSION['senha_alterada'] = true;
		}
		else {
			$_SESSION['senha_incorreta'] = true;
		}
		$conn = Database::closeConn();	

	} elseif (isset($_POST['confirm-info'])) {
		$conn = Database::openConn();

		$_POST['inf_id'] = $_SESSION['profile'][0]['inf_id'];
		$_POST['inf_data_nascimento'] = substr($_POST['inf_data_nascimento'], 6, 4).'-'.substr($_POST['inf_data_nascimento'], 3, 2).'-'.substr($_POST['inf_data_nascimento'], 0, 2);
		$func =  Funcionario::updateFuncionarioInfo($_POST, $conn);

		$_SESSION['profile'][0]['inf_nome'] = $_POST['inf_nome'];
		$_SESSION['profile'][0]["inf_data_nascimento"] = $_POST["inf_data_nascimento"];
		$_SESSION['profile'][0]["inf_rg"] = $_POST["inf_rg"];
		$_SESSION['profile'][0]["inf_cpf"] = $_POST["inf_cpf"];
		$_SESSION['profile'][0]["inf_ctps"] = $_POST["inf_ctps"];
		$_SESSION['profile'][0]["inf_endereco"] = $_POST["inf_endereco"];
		$_SESSION['profile'][0]["inf_cidade"] = $_POST["inf_cidade"];
		$_SESSION['profile'][0]["inf_uf"] = $_POST["inf_uf"];
		$_SESSION['profile'][0]["inf_cep"] = $_POST["inf_cep"];
		$_SESSION['profile'][0]["inf_email"] = $_POST["inf_email"];
		$_SESSION['profile'][0]["inf_tel_fixo"] = $_POST["inf_tel_fixo"];
		$_SESSION['profile'][0]["inf_tel_celular"] = $_POST["inf_tel_celular"];

		$_SESSION['info_alterada'] = true;

		$conn = Database::closeConn();
	}elseif (isset($_POST['confirm-banc'])) {
		$conn = Database::openConn();

		$banco_func = DadosBancoFuncionario::readDadosBancoFuncionario($_SESSION['profile'][0]['inf_id'], $conn)[0];
		$banc =  new DadosBancarios($_POST);
		$banc->updateDadosBancarios($banco_func['DadosBancarios_bnc_id'], $conn);

		$_SESSION['profile'][0]["bnc_nome"] = $_POST["bnc_nome"];
		$_SESSION['profile'][0]["bnc_numero"] = $_POST["bnc_numero"];
		$_SESSION['profile'][0]["bnc_agencia"] = $_POST["bnc_agencia"];
		$_SESSION['profile'][0]["bnc_operacao"] = $_POST["bnc_operacao"];
		$_SESSION['profile'][0]["bnc_conta"] = $_POST["bnc_conta"];
		$_SESSION['profile'][0]["bnc_contato"] = $_POST["bnc_contato"];
		$_SESSION['profile'][0]["bnc_telefone"] = $_POST["bnc_telefone"];

		$_SESSION['banc_alterada'] = true;

		$conn = Database::closeConn();
	}elseif (isset($_POST['confirm-spec'])) {
		$conn = Database::openConn();

		$_POST['inf_id'] = $_SESSION['profile'][0]['inf_id'];
		$func =  Funcionario::updateFuncionarioSpec($_POST, $conn);

		if($_SESSION['profile'][0]['TipoFuncionario_tfn_id']==1){
			$_SESSION['profile'][0]["est_curso"] = $_POST["est_curso"];
			$_SESSION['profile'][0]["est_periodo"] = $_POST["est_periodo"];
			$_SESSION['profile'][0]["est_prev_conclusao"] = $_POST["est_prev_conclusao"];
			$_SESSION['profile'][0]["est_termo"] = $_POST["est_termo"];
			$_SESSION['profile'][0]["est_empresa"] = $_POST["est_empresa"];
			$_SESSION['profile'][0]["est_data_inicio"] = $_POST["est_data_inicio"];
			$_SESSION['profile'][0]["est_data_termino"] = $_POST["est_data_termino"];
			$_SESSION['profile'][0]["est_carga_horaria"] = $_POST["est_carga_horaria"];
			$_SESSION['profile'][0]["est_cond_estagio"] = $_POST["est_cond_estagio"];
			$_SESSION['profile'][0]["est_seguro"] = $_POST["est_seguro"];
			$_SESSION['profile'][0]["est_benef_bolsa"] = $_POST["est_benef_bolsa"];
			$_SESSION['profile'][0]["est_benef_transporte"] = $_POST["est_benef_transporte"];
			$_SESSION['profile'][0]["est_benef_refeicao"] = $_POST["est_benef_refeicao"];
		}else{
			$_SESSION['profile'][0]["fnc_pis"] = $_POST["fnc_pis"];
		}

		$_SESSION['spec_alterada'] = true;
		$conn = Database::closeConn();
	}

	header('Location: ../views/perfil.php');
?>