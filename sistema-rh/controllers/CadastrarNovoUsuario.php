<?php
	require_once('../models/Database.php');
	require_once('../models/Funcionario.php');
	require_once('../models/AcessoFuncionario.php');
	
	
	session_start();

	$matricula = $_POST['matricula'];

	$dados = array();
	
	$dados['acf_login'] = $matricula;
	$dados['acf_senha'] = md5($matricula);
	$dados['acf_nivel_acesso'] = $_POST['acf_nivel_acesso'];
	$dados['Funcionario_inf_id'] = substr($matricula, 3);

	$conn = Database::openConn();
	$acesso = new AcessoFuncionario($dados);
	$acesso->createAcessoFuncionario($conn);

	$conn = Database::closeConn();
	
	$_SESSION['criado'] = true;
	header("Location: ../views/consultarFuncionario.php");
	die();
?>