<?php 

	require_once('../models/Database.php');
	require_once('../models/Funcionario.php');
	require_once('../models/TipoFuncionario.php');
	require_once('../models/DadosBancarios.php');
	require_once('../models/DadosBancoFuncionario.php');
	require_once('../models/AcessoFuncionario.php');

	session_start();
	$id = $_POST['btn_deletar_confirm'];

	$conn = Database::openConn();
	$banco_func = DadosBancoFuncionario::readDadosBancoFuncionario($id, $conn);

	$delbdf = DadosBancoFuncionario::deleteDadosBancoFuncionario($banco_func[0]['bfn_id'], $conn);
	$del_db = DadosBancarios::deleteDadosBancarios($banco_func[0]['DadosBancarios_bnc_id'], $conn);

	$acesso_func = AcessoFuncionario::readAcessoFuncionario($id,$conn);
	if($acesso_func){
		AcessoFuncionario::deleteAcessoFuncionario($acesso_func[0]['acf_id'], $conn);
	}

	Funcionario::deleteFuncionario($id, $conn);
	$conn = Database::closeConn();

	$_SESSION['deletado'] = true;
	header("Location: ../views/consultarFuncionario.php");

?>