<?php

	require_once('../models/Database.php');
	require_once('../models/Funcionario.php');
	require_once('../models/TipoFuncionario.php');

	$busca = trim($_POST['text_busca']);
	$result = array();

	$conn = Database::openConn();
	$array_func = Funcionario::readAllFuncionario($conn);
	$conn = Database::closeConn();

	//var_dump($array_func);
	
	foreach ($array_func as $i)
	{
		if(stripos($i['inf_nome'], $busca)!==false)
		{
			array_push($result, $i);
		}		
	}
	session_start();
	$_SESSION['result'] = array_reverse($result);

	header('Location:../views/consultarFuncionario.php');

?>