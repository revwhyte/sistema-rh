<?php 
	
	require_once('../models/Database.php');
	require_once('../models/Funcionario.php');
	require_once('../models/DadosBancarios.php');
	require_once('../models/TipoFuncionario.php');
	require_once('../models/DadosBancoFuncionario.php');

	session_start();
	
	$conn = Database::openConn();
	$_POST['inf_data_nascimento'] = substr($_POST['inf_data_nascimento'], 6, 4).'-'.substr($_POST['inf_data_nascimento'], 3, 2).'-'.substr($_POST['inf_data_nascimento'], 0, 2); 
	$func = Funcionario::updateFuncionario($_POST, $conn);

	$dbf = DadosBancoFuncionario::readDadosBancoFuncionario($_POST['inf_id'], $conn)[0];

	$dbc = new DadosBancarios($_POST, $conn);
	$dbc->updateDadosBancarios($dbf['DadosBancarios_bnc_id'], $conn);

	$conn = Database::closeConn();

	$_SESSION['editado'] = true;
	header('Location: ../views/consultarFuncionario.php');

?>