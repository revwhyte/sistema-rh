<?php

	require_once('../models/AcessoFuncionario.php');
	require_once('../models/Funcionario.php');
	require_once('../models/TipoFuncionario.php');
	require_once('../models/Database.php');	
	require_once('../models/DadosBancarios.php');
	require_once('../models/DadosBancoFuncionario.php');

	$login = $_POST['login'];
	$senha = md5($_POST['senha']);

	$conn = Database::openConn();
	$usr = AcessoFuncionario::login($login, $senha, $conn);
	$conn = Database::closeConn();

	session_start();

	if($usr['status_login'])
	{
		$_SESSION['logged'] = true;
		//
		if(isset($usr['status_login']['Funcionario_inf_id'])){
			$conn = Database::openConn();
			$_SESSION['profile'] = Funcionario::readFuncionarioId($usr['status_login']['Funcionario_inf_id'], $conn);
			$banco_func = DadosBancoFuncionario::readDadosBancoFuncionario($usr['status_login']['Funcionario_inf_id'], $conn);
			$dados_banc = DadosBancarios::readDadosBancarios($banco_func[0]['DadosBancarios_bnc_id'], $conn);

			$acesso_func = AcessoFuncionario::readAcessoFuncionario($usr['status_login']['Funcionario_inf_id'], $conn);
			$conn = Database::closeConn();

			$_SESSION['profile'][0] = array_merge($_SESSION['profile'][0], $dados_banc[0]);
			$_SESSION['profile'][0] = array_merge($_SESSION['profile'][0], $acesso_func);			

		}
		//
		header('Location:../views/main.php');
	}

	else
	{
		$_SESSION['logged'] = false;
		header('Location:../views/home.php');
	}

?>