<?php
	
	require_once('../models/Database.php');
	require_once('../models/Funcionario.php');
	require_once('../models/DadosBancarios.php');
	require_once('../models/TipoFuncionario.php');
	require_once('../models/DadosBancoFuncionario.php');
	
	session_start();

	// Verifica se funcionário com o CPF em questão já existe

	$conn = Database::openConn();
	if(Funcionario::readFuncionario($_POST['inf_cpf'], $conn))
	{
		$_SESSION['cpf_usado'] = true;
		header('Location: ../views/cadastrarFuncionario.php');
		die();
	}
	if(DadosBancarios::readDadosBancariosAgConta($_POST['bnc_agencia'],$_POST['bnc_conta'], $conn))
	{
		$_SESSION['conta_usada'] = true;
		header('Location: ../views/cadastrarFuncionario.php');
		die();
	}
	$conn = Database::closeConn();

	$dados = array();
	$banco = array();
	$especificos = array();
	$id_tipo_func = 0;

	/* Define tipo de funcionário:
	1 - estagiário
	2 - funcionário
	3 - prestador de serviços */

	if(isset($_POST['check_estag']))
	{
		unset($_POST['check_estag']);
		$id_tipo_func = 1;
	}
	else if(isset($_POST['tipo_func']) && $_POST['tipo_func'] == "option1")
	{
		$id_tipo_func = 2;
	}
	else
	{
		$id_tipo_func = 3;
	}
	unset($_POST['tipo_func']);

	/* Elimina a referência do checkbox de estagiário e trata 
	os valores de formulário referentes aos dados comuns */

	$_POST['inf_data_nascimento'] = substr($_POST['inf_data_nascimento'], 6, 4).'-'.substr($_POST['inf_data_nascimento'], 3, 2).'-'.substr($_POST['inf_data_nascimento'], 0, 2); 
	//Converte a data no formato do banco.

	$dados = array_slice($_POST, 0, 12);
	for($i = 0; $i < count($dados); $i++)
	{
		array_shift($_POST);
	}

	/* Retira os dados comuns de $_POST e trata os dados de
	formulário referentes aos dados bancários do funcionário em cadastro */

	$banco = array_slice($_POST, 0, 7);
	for($i = 0; $i < count($banco); $i++)
	{
		array_shift($_POST);
	}

	if($id_tipo_func != 1) // funcionário ou prestador de serviços
	{
		$especificos['fnc_pis'] = $_POST['fnc_pis'];
	}
	else
	{
		$especificos = array_slice($_POST, 1);
	}

	$conn = Database::openConn();

	$func = new Funcionario($dados, $especificos, $id_tipo_func);
	$func->createFuncionario($conn);

	$bnc = new DadosBancarios($banco);
	$bnc->createDadosBancarios($conn);

	$func = Funcionario::readFuncionario($dados['inf_cpf'], $conn);
	$bnc = DadosBancarios::readDadosBancariosAgConta($banco['bnc_agencia'], $banco['bnc_conta'], $conn);

	$ar = array();

	$new_mat = array('EST' => 1, 'FNC' => 2, 'PST' => 3);

	$ar['inf_id'] = $func[0]['inf_id'];
	$ar['bnc_id'] = $bnc[0]['bnc_id'];
	$ar['prefixo'] = array_search($func[0]['TipoFuncionario_tfn_id'], $new_mat) . $func[0]['inf_id'];

	$dbf = new DadosBancoFuncionario($ar);
	$dbf->createDadosBancoFuncionario($conn);

	Funcionario::updateFuncionarioMatricula($ar['prefixo'], $ar['inf_id'], $conn);

	$conn = Database::closeConn();
	
	$_SESSION['sucesso'] = true;
	header('Location: ../views/cadastrarFuncionario.php');
	
?>