<?php
	
	require_once('../models/Database.php');
	require_once('../models/AcessoFuncionario.php');
	
	session_start();

	$conn = Database::openConn();

	$acs = AcessoFuncionario::readAcessoFuncionario($_POST['btn_deletar_user'], $conn);
	$del = AcessoFuncionario::deleteAcessoFuncionario($acs[0]['acf_id'], $conn);

	$conn = Database::closeConn();

	$_SESSION['user_deletado'] = true;
	header('Location: ../views/consultarFuncionario.php');

?>