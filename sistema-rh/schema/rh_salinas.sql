CREATE DATABASE `rh_salinas`;
USE `rh_salinas`;

CREATE TABLE IF NOT EXISTS `rh_salinas`.`TipoFuncionario` (
  `tfn_id` INT NOT NULL AUTO_INCREMENT,
  `tfn_nome_tipo` VARCHAR(45) NOT NULL,
  `tfn_sigla` CHAR(3) NOT NULL,
  PRIMARY KEY (`tfn_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `rh_salinas`.`Funcionario` (
  `inf_id` INT NOT NULL AUTO_INCREMENT,
  `inf_nome` VARCHAR(60) NOT NULL,
  `inf_data_nascimento` DATE NOT NULL,
  `inf_rg` VARCHAR(11) NOT NULL,
  `inf_cpf` VARCHAR(14) NOT NULL,
  `inf_ctps` VARCHAR(14) NOT NULL,
  `inf_endereco` VARCHAR(60) NOT NULL,
  `inf_cidade` VARCHAR(38) NOT NULL,
  `inf_uf` VARCHAR(2) NOT NULL,
  `inf_cep` VARCHAR(10) NOT NULL,
  `inf_email` VARCHAR(60) NOT NULL,
  `inf_tel_fixo` VARCHAR(16) NOT NULL,
  `inf_tel_celular` VARCHAR(16) NOT NULL,
  `inf_matricula` VARCHAR(7) NOT NULL,
  `fnc_pis` VARCHAR(14) NULL,
  `est_curso` VARCHAR(80) NULL,
  `est_periodo` VARCHAR(6) NULL,
  `est_prev_conclusao` VARCHAR(6) NULL,
  `est_termo` VARCHAR(60) NULL,
  `est_empresa` VARCHAR(32) NULL,
  `est_data_inicio` DATE NULL,
  `est_data_termino` DATE NULL,
  `est_carga_horaria` VARCHAR(4) NULL,
  `est_cond_estagio` VARCHAR(17) NULL,
  `est_seguro` VARCHAR(7) NULL,
  `est_benef_bolsa` FLOAT NULL,
  `est_benef_transporte` FLOAT NULL,
  `est_benef_refeicao` FLOAT NULL,
  `TipoFuncionario_tfn_id` INT NOT NULL,
  PRIMARY KEY (`inf_id`),
  INDEX `fk_Funcionario_TipoFuncionario_idx` (`TipoFuncionario_tfn_id` ASC),
  INDEX `fun_cpf` (`inf_cpf` ASC),
  UNIQUE INDEX `inf_cpf_UNIQUE` (`inf_cpf` ASC),
  CONSTRAINT `fk_Funcionario_TipoFuncionario`
    FOREIGN KEY (`TipoFuncionario_tfn_id`)
    REFERENCES `rh_salinas`.`TipoFuncionario` (`tfn_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `rh_salinas`.`AcessoFuncionario` (
  `acf_id` INT NOT NULL AUTO_INCREMENT,
  `acf_login` VARCHAR(7) NOT NULL COMMENT 'Replica a matricla já cadastrada na tabela Funcionario',
  `acf_senha` VARCHAR(32) NOT NULL COMMENT 'Senha definida pelo adm, criar funcionalidade para alteração de senha',
  `acf_nivel_acesso` INT NOT NULL COMMENT 'Definir padrões de nível de acesso... Criar tabela para esses padroes',
  `Funcionario_inf_id` INT NULL COMMENT 'Ligação com Funcionario',
  PRIMARY KEY (`acf_id`),
  INDEX `fk_AcessoFuncionario_Funcionario1_idx` (`Funcionario_inf_id` ASC),
  CONSTRAINT `fk_AcessoFuncionario_Funcionario1`
    FOREIGN KEY (`Funcionario_inf_id`)
    REFERENCES `rh_salinas`.`Funcionario` (`inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `rh_salinas`.`DadosBancarios` (
  `bnc_id` INT NOT NULL AUTO_INCREMENT,
  `bnc_nome` VARCHAR(35) NOT NULL,
  `bnc_numero` VARCHAR(10) NULL,
  `bnc_agencia` VARCHAR(10) NOT NULL,
  `bnc_operacao` VARCHAR(3) NULL,
  `bnc_conta` VARCHAR(12) NOT NULL,
  `bnc_contato` VARCHAR(15) NULL,
  `bnc_telefone` VARCHAR(16) NULL,
  PRIMARY KEY (`bnc_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = ' ';

CREATE TABLE IF NOT EXISTS `rh_salinas`.`BancoFuncionario` (
  `bfn_id` INT NOT NULL AUTO_INCREMENT,
  `Funcionario_inf_id` INT NOT NULL,
  `DadosBancarios_bnc_id` INT NOT NULL,
  PRIMARY KEY (`bfn_id`),
  INDEX `fk_bancos_funcionario_Funcionario1_idx` (`Funcionario_inf_id` ASC),
  INDEX `fk_bancos_funcionario_Dados_bancarios1_idx` (`DadosBancarios_bnc_id` ASC),
  CONSTRAINT `fk_bancos_funcionario_Funcionario1`
    FOREIGN KEY (`Funcionario_inf_id`)
    REFERENCES `rh_salinas`.`Funcionario` (`inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bancos_funcionario_Dados_bancarios1`
    FOREIGN KEY (`DadosBancarios_bnc_id`)
    REFERENCES `rh_salinas`.`DadosBancarios` (`bnc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

