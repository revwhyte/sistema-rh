<?php

	require_once('Database.php');

	class DadosBancoFuncionario
	{
		private $id_funcionario;
		private $id_banco;
		
		public function __construct($ids)
		{
			$this->id_funcionario = $ids['inf_id'];
			$this->id_banco = $ids['bnc_id'];
		}

		public function createDadosBancoFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->prepare("INSERT INTO BancoFuncionario(Funcionario_inf_id, 
							DadosBancarios_bnc_id)
							dld111VALUES(:func, :banco)");

				$sth->bindParam(":func", $this->id_funcionario, PDO::PARAM_INT);
				$sth->bindParam(":banco", $this->id_banco, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readDadosBancoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT bfn_id, Funcionario_inf_id, DadosBancarios_bnc_id
										FROM BancoFuncionario WHERE Funcionario_inf_id = :id");

				$sth->bindParam(":id", $id, PDO::PARAM_INT);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readAllDadosBancoFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->query("SELECT Funcionario_inf_id, DadosBancarios_bnc_id
									FROM BancoFuncionario ORDER BY bfn_id ASC");

				$sth->execute();
				$results = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $results;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public function updateDadosBancoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("UPDATE BancoFuncionario SET 
										Funcionario_inf_id = :func, DadosBancarios_bnc_id = :banco
										WHERE bfn_id = :id");

				$sth->bindParam(":func", $this->id_funcionario, PDO::PARAM_INT);
				$sth->bindParam(":banco", $this->id_banco, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function deleteDadosBancoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("DELETE FROM BancoFuncionario WHERE bfn_id = :id LIMIT 1");
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}
	}
?>