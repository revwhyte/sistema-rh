<?php

	require_once('Database.php');

	class DadosBancarios
	{
		private $nome;
		private $numero;
		private $agencia;
		private $operacao;
		private $conta;
		private $contato;
		private $telefone;

		public function __construct($dados)
		{
			$this->nome = $dados['bnc_nome'];
			$this->numero = $dados['bnc_numero'];
			$this->agencia = $dados['bnc_agencia'];
			$this->operacao = $dados['bnc_operacao'];
			$this->conta = $dados['bnc_conta'];
			$this->contato = $dados['bnc_contato'];
			$this->telefone = $dados['bnc_telefone'];
		}

		public function createDadosBancarios($dbh)
		{
			try
			{
				$sth = $dbh->prepare(
					"INSERT INTO DadosBancarios(bnc_nome, 
								bnc_numero, 
								bnc_agencia, 
								bnc_operacao, 
								bnc_conta, 
								bnc_contato, 
								bnc_telefone) 
					VALUES(:nome, 
							:num, 
							:ag, 
							:op, 
							:conta, 
							:contato, 
							:tel)");

				$sth->bindParam(":nome", $this->nome, PDO::PARAM_STR);
				$sth->bindParam(":num", $this->numero, PDO::PARAM_STR);
				$sth->bindParam(":ag", $this->agencia, PDO::PARAM_STR);
				$sth->bindParam(":op", $this->operacao, PDO::PARAM_STR);
				$sth->bindParam(":conta", $this->conta, PDO::PARAM_STR);
				$sth->bindParam(":contato", $this->contato, PDO::PARAM_STR);
				$sth->bindParam(":tel", $this->telefone, PDO::PARAM_STR);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readDadosBancarios($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT bnc_id, 
											bnc_nome, 
											bnc_numero, 
											bnc_agencia, 
											bnc_operacao, 
											bnc_conta, 
											bnc_contato, 
											bnc_telefone
										FROM DadosBancarios WHERE bnc_id = :id");

				$sth->bindParam(":id", $id, PDO::PARAM_INT);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readDadosBancariosAgConta($agencia, $conta, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT bnc_id, 
											bnc_nome, 
											bnc_numero, 
											bnc_agencia, 
											bnc_operacao, 
											bnc_conta, 
											bnc_contato, 
											bnc_telefone
										FROM DadosBancarios WHERE bnc_agencia = :ag AND bnc_conta = :conta");

				$sth->bindParam(":ag", $agencia, PDO::PARAM_STR);
				$sth->bindParam(":conta", $conta, PDO::PARAM_STR);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readAllDadosBancarios($dbh)
		{
			try
			{
				$sth = $dbh->query("SELECT bnc_nome, 
										bnc_numero, 
										bnc_agencia, 
										bnc_operacao, 
										bnc_conta, 
										bnc_contato, 
										bnc_telefone
									FROM DadosBancarios ORDER BY bnc_id ASC");

				$sth->execute();
				$results = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $results;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public function updateDadosBancarios($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("UPDATE DadosBancarios SET bnc_nome = :nome, 
								bnc_numero = :num, 
								bnc_agencia = :ag, 
								bnc_operacao = :op, 
								bnc_conta = :conta, 
								bnc_contato = :contato, 
								bnc_telefone = :tel WHERE bnc_id = :id");

				$sth->bindParam(":nome", $this->nome, PDO::PARAM_STR);
				$sth->bindParam(":num", $this->numero, PDO::PARAM_STR);
				$sth->bindParam(":ag", $this->agencia, PDO::PARAM_STR);
				$sth->bindParam(":op", $this->operacao, PDO::PARAM_STR);
				$sth->bindParam(":conta", $this->conta, PDO::PARAM_STR);
				$sth->bindParam(":contato", $this->contato, PDO::PARAM_STR);
				$sth->bindParam(":tel", $this->telefone, PDO::PARAM_STR);
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function deleteDadosBancarios($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("DELETE FROM DadosBancarios WHERE bnc_id = :id LIMIT 1");
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}
	}
?>