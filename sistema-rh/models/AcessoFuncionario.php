<?php

	require_once('Database.php');

	class AcessoFuncionario
	{
		private $login;
		private $senha;
		private $nivel_acesso;
		private $Funcionario_inf_id;

		public function __construct($dados)
		{
			$this->login = $dados['acf_login'];
			$this->senha = $dados['acf_senha'];
			$this->nivel_acesso = $dados['acf_nivel_acesso'];
			$this->Funcionario_inf_id = $dados['Funcionario_inf_id'];
		}

		public function createAcessoFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->prepare(
					"INSERT INTO AcessoFuncionario(acf_login, 
								acf_senha, 
								acf_nivel_acesso, 
								Funcionario_inf_id) 
					VALUES(:login, 
							:senha, 
							:nivel, 
							:fun_id)");

				$sth->bindParam(":login", $this->login, PDO::PARAM_STR);
				$sth->bindParam(":senha", $this->senha, PDO::PARAM_STR);
				$sth->bindParam(":nivel", $this->nivel_acesso, PDO::PARAM_STR);
				$sth->bindParam(":fun_id", $this->Funcionario_inf_id, PDO::PARAM_STR);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readAcessoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT acf_id,
											acf_login, 
											acf_senha, 
											acf_nivel_acesso, 
											Funcionario_inf_id
										FROM AcessoFuncionario WHERE Funcionario_inf_id = :id");

				$sth->bindParam(":id", $id, PDO::PARAM_INT);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readAllAcessoFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->query("SELECT acf_login, 
											acf_senha, 
											acf_nivel_acesso, 
											Funcionario_inf_id
									FROM AcessoFuncionario ORDER BY acf_id ASC");

				$sth->execute();
				$results = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $results;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public function updateAcessoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("UPDATE AcessoFuncionario SET acf_senha = :senha 
								 WHERE acf_id = :id");

				$sth->bindParam(":senha", $this->senha, PDO::PARAM_STR);
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function deleteAcessoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("DELETE FROM AcessoFuncionario WHERE acf_id = :id LIMIT 1");
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		static function login($login, $senha, $conn)
		{
			$result = null;
			
			try
			{
				$sql = $conn->prepare("SELECT acf_id,
										acf_login,
										acf_senha,
										acf_nivel_acesso,
										Funcionario_inf_id
									 FROM AcessoFuncionario WHERE acf_login = :login AND acf_senha = :senha");
				$sql->bindParam(":login", $login, PDO::PARAM_STR);
				$sql->bindParam(":senha", $senha, PDO::PARAM_STR);
				
				if($sql->execute())
				{ 
					$result['status_login'] = $sql->fetch(PDO::FETCH_ASSOC);

					// if($result)
					// {
					// 	try
					// 	{
					// 		$foreign_key = ":" . $result['status_login']['Funcionario_inf_id'];
					// 		$sql = $conn->prepare("SELECT * FROM Funcionario WHERE inf_id = " . $foreign_key);

					// 		if($sql->execute())
					// 		{
					// 			$result['user_info'] = $sql->fetch(PDO::FETCH_ASSOC);
					// 		}
							
					// 		return $result;
					// 	}
						
					// 	catch (PDOException $e)
					// 	{
					// 		$e->getMessage();
					// 	}
					// }

					return $result;

				}

				return false;

			}

			catch (PDOException $e)
			{
				$e->getMessage();
				
			}

			return false;

		}
		
	}

?>