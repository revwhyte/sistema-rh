<?php

	require_once('Database.php');

	class Funcionario
	{
		private $nome;
		private $data_nascimento;
		private $rg;
		private $cpf;
		private $ctps;
		private $endereco;
		private $cidade;
		private $uf;
		private $cep;
		private $email;
		private $tel_fixo;
		private $tel_celular;
		private $matricula;
		private $pis;
		private $curso;
		private $periodo;
		private $prev_conclusao;
		private $termo;
		private $empresa;
		private $data_inicio;
		private $data_termino;
		private $carga_horaria;
		private $cond_estagio;
		private $seguro;
		private $benef_bolsa;
		private $benef_transporte;
		private $benef_refeicao;
		private $TipoFuncionario_tfn_id;

		public function __construct($dados, $especificos, $id_tipo_func)
		{
			$this->nome = $dados['inf_nome'];
			$this->data_nascimento = $dados['inf_data_nascimento'];
			$this->rg = $dados['inf_rg'];
			$this->cpf = $dados['inf_cpf'];
			$this->ctps = $dados['inf_ctps'];
			$this->endereco = $dados['inf_endereco'];
			$this->cidade = $dados['inf_cidade'];
			$this->uf = $dados['inf_uf'];
			$this->cep = $dados['inf_cep'];
			$this->email = $dados['inf_email'];
			$this->tel_fixo = $dados['inf_tel_fixo'];
			$this->tel_celular = $dados['inf_tel_celular'];
			$this->matricula = null;

			if(isset($especificos['fnc_pis']))
			{
				$this->pis = $especificos['fnc_pis'];				
			}
			else
			{
				$this->curso = $especificos['est_curso'];
				$this->periodo = $especificos['est_periodo'];
				$this->prev_conclusao = $especificos['est_prev_conclusao'];
				$this->termo = $especificos['est_termo'];
				$this->empresa = $especificos['est_empresa'];
				$this->data_inicio = $especificos['est_data_inicio'];
				$this->data_termino = $especificos['est_data_termino'];
				$this->carga_horaria = $especificos['est_carga_horaria'];
				if(isset($especificos['est_cond_estagio']))
				{
					$this->cond_estagio = $especificos['est_cond_estagio'];
				}				
				$this->seguro = $especificos['est_seguro'];
				if(isset($especificos['est_benef_bolsa']))
				{
					$this->benef_bolsa = $especificos['est_benef_bolsa'];	
				}
				if(isset($especificos['est_benef_transporte']))
				{
					$this->benef_transporte = $especificos['est_benef_transporte'];
				}
				if(isset($especificos['est_benef_refeicao']))
				{
					$this->benef_refeicao = $especificos['est_benef_refeicao'];
				}
			}
			$this->TipoFuncionario_tfn_id = $id_tipo_func;
		}

		public function createFuncionario($dbh)
		{
			try
			{
				if($this->TipoFuncionario_tfn_id == 1)
				{
					$sth = $dbh->prepare(
						"INSERT INTO Funcionario(inf_nome, inf_data_nascimento, 
									inf_rg, inf_cpf, 
									inf_ctps, inf_endereco, 
									inf_cidade, inf_uf, 
									inf_cep, inf_email, 
									inf_tel_fixo, inf_tel_celular, 
									est_curso, est_periodo, est_prev_conclusao, 
									est_termo, est_empresa, 
									est_data_inicio, est_data_termino, 
									est_carga_horaria, est_cond_estagio, 
									est_seguro, est_benef_bolsa, 
									est_benef_transporte, est_benef_refeicao, 
									TipoFuncionario_tfn_id)
						VALUES(:nome, :nasc, :rg, :cpf, :ctps, :endereco, 
								:cidade, :uf, :cep, :email, :fixo, 
								:celular, :curso, :periodo, :conclusao, 
								:termo, :empresa, :inicio, :termino, :carga, 
								:condicao, :seguro, :bolsa, :transporte, :refeicao, :tipo)");

					$sth->bindParam(":nome", $this->nome, PDO::PARAM_STR);
					$sth->bindParam(":nasc", $this->data_nascimento, PDO::PARAM_STR);
					$sth->bindParam(":rg", $this->rg, PDO::PARAM_STR);
					$sth->bindParam(":cpf", $this->cpf, PDO::PARAM_STR);
					$sth->bindParam(":ctps", $this->ctps, PDO::PARAM_STR);
					$sth->bindParam(":endereco", $this->endereco, PDO::PARAM_STR);
					$sth->bindParam(":cidade", $this->cidade, PDO::PARAM_STR);
					$sth->bindParam(":uf", $this->uf, PDO::PARAM_STR);
					$sth->bindParam(":cep", $this->cep, PDO::PARAM_STR);
					$sth->bindParam(":email", $this->email, PDO::PARAM_STR);
					$sth->bindParam(":fixo", $this->tel_fixo, PDO::PARAM_STR);
					$sth->bindParam(":celular", $this->tel_celular, PDO::PARAM_STR);

					$sth->bindParam(":curso", $this->curso, PDO::PARAM_STR);
					$sth->bindParam(":periodo", $this->periodo, PDO::PARAM_STR);
					$sth->bindParam(":conclusao", $this->prev_conclusao, PDO::PARAM_STR);
					$sth->bindParam(":termo", $this->termo, PDO::PARAM_STR);
					$sth->bindParam(":empresa", $this->empresa, PDO::PARAM_STR);
					$sth->bindParam(":inicio", $this->data_inicio, PDO::PARAM_STR);
					$sth->bindParam(":termino", $this->data_termino, PDO::PARAM_STR);
					$sth->bindParam(":carga", $this->carga_horaria, PDO::PARAM_STR);
					$sth->bindParam(":condicao", $this->cond_estagio, PDO::PARAM_STR);
					$sth->bindParam(":seguro", $this->seguro, PDO::PARAM_STR);
					$sth->bindParam(":bolsa", $this->benef_bolsa, PDO::PARAM_STR);
					$sth->bindParam(":transporte", $this->benef_transporte, PDO::PARAM_STR);
					$sth->bindParam(":refeicao", $this->benef_refeicao, PDO::PARAM_STR);

					$sth->bindParam(":tipo", $this->TipoFuncionario_tfn_id, PDO::PARAM_INT);
					
				}
				else
				{
					$sth = $dbh->prepare(
						"INSERT INTO Funcionario(inf_nome, inf_data_nascimento, 
									inf_rg, inf_cpf, 
									inf_ctps, inf_endereco, 
									inf_cidade, inf_uf, 
									inf_cep, inf_email, 
									inf_tel_fixo, inf_tel_celular, 
									fnc_pis, TipoFuncionario_tfn_id)
						VALUES(:nome, :nasc, :rg, :cpf, :ctps, :endereco, 
								:cidade, :uf, :cep, :email, :fixo, 
								:celular, :pis, :tipo)");

					$sth->bindParam(":nome", $this->nome, PDO::PARAM_STR);
					$sth->bindParam(":nasc", $this->data_nascimento, PDO::PARAM_STR);
					$sth->bindParam(":rg", $this->rg, PDO::PARAM_STR);
					$sth->bindParam(":cpf", $this->cpf, PDO::PARAM_STR);
					$sth->bindParam(":ctps", $this->ctps, PDO::PARAM_STR);
					$sth->bindParam(":endereco", $this->endereco, PDO::PARAM_STR);
					$sth->bindParam(":cidade", $this->cidade, PDO::PARAM_STR);
					$sth->bindParam(":uf", $this->uf, PDO::PARAM_STR);
					$sth->bindParam(":cep", $this->cep, PDO::PARAM_STR);
					$sth->bindParam(":email", $this->email, PDO::PARAM_STR);
					$sth->bindParam(":fixo", $this->tel_fixo, PDO::PARAM_STR);
					$sth->bindParam(":celular", $this->tel_celular, PDO::PARAM_STR);

					$sth->bindParam(":pis", $this->pis, PDO::PARAM_STR);

					$sth->bindParam(":tipo", $this->TipoFuncionario_tfn_id, PDO::PARAM_INT);

					
				}

				return $sth->execute();
			}

			catch (PDOException $e)
			{
				echo $e->getMessage();				
			}
		}

		public static function readFuncionario($cpf, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT inf_id, 
									inf_nome, 
									inf_data_nascimento, 
									inf_rg, 
									inf_cpf, 
									inf_ctps, 
									inf_endereco, 
									inf_cidade, 
									inf_uf, 
									inf_cep, 
									inf_email, 
									inf_tel_fixo, 
									inf_tel_celular,
									inf_matricula, 
									fnc_pis, 
									est_curso, 
									est_periodo, 
									est_prev_conclusao, 
									est_termo, 
									est_empresa, 
									est_data_inicio, 
									est_data_termino, 
									est_carga_horaria, 
									est_cond_estagio, 
									est_seguro, 
									est_benef_bolsa, 
									est_benef_transporte, 
									est_benef_refeicao, 
									TipoFuncionario_tfn_id
								FROM Funcionario WHERE inf_cpf = :cpf");
				
				$sth->bindParam(":cpf", $cpf, PDO::PARAM_STR);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readFuncionarioId($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT inf_id, 
									inf_nome, 
									inf_data_nascimento, 
									inf_rg, 
									inf_cpf, 
									inf_ctps, 
									inf_endereco, 
									inf_cidade, 
									inf_uf, 
									inf_cep, 
									inf_email, 
									inf_tel_fixo, 
									inf_tel_celular,
									inf_matricula, 
									fnc_pis, 
									est_curso, 
									est_periodo, 
									est_prev_conclusao, 
									est_termo, 
									est_empresa, 
									est_data_inicio, 
									est_data_termino, 
									est_carga_horaria, 
									est_cond_estagio, 
									est_seguro, 
									est_benef_bolsa, 
									est_benef_transporte, 
									est_benef_refeicao, 
									TipoFuncionario_tfn_id
								FROM Funcionario WHERE inf_id = :id");
				
				$sth->bindParam(":id", $id, PDO::PARAM_STR);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readFuncionarioEmail($email, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT inf_id FROM Funcionario WHERE inf_email = :email");
				$sth->bindParam(":email", $email, PDO::PARAM_STR);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function readAllFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->query("SELECT inf_id, 
									inf_nome, 
									inf_data_nascimento, 
									inf_rg, 
									inf_cpf, 
									inf_ctps, 
									inf_endereco, 
									inf_cidade, 
									inf_uf, 
									inf_cep, 
									inf_email, 
									inf_tel_fixo, 
									inf_tel_celular, 
									inf_matricula, 
									fnc_pis, 
									est_curso, 
									est_periodo, 
									est_prev_conclusao, 
									est_termo, 
									est_empresa, 
									est_data_inicio, 
									est_data_termino, 
									est_carga_horaria, 
									est_cond_estagio, 
									est_seguro, 
									est_benef_bolsa, 
									est_benef_transporte, 
									est_benef_refeicao, 
									TipoFuncionario_tfn_id
								FROM Funcionario ORDER BY inf_nome ASC");

				$sth->execute();
				$results = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $results;
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function updateFuncionario($info, $dbh)
		{
			try
			{
				if(isset($info['fnc_pis']))
				{
					$sth = $dbh->prepare("UPDATE Funcionario SET inf_nome = :nome, 
										inf_data_nascimento = :nasc, 
										inf_rg = :rg, inf_cpf = :cpf, 
										inf_ctps = :ctps, inf_endereco = :endereco, 
										inf_cidade = :cidade, 
										inf_uf = :uf, inf_cep = :cep, 
										inf_email = :email, 
										inf_tel_fixo = :fixo, 
										inf_tel_celular = :celular, 
										fnc_pis = :pis WHERE inf_id = :id");

					$sth->bindParam(":nome", $info['inf_nome'], PDO::PARAM_STR);
					$sth->bindParam(":nasc", $info['inf_data_nascimento'], PDO::PARAM_STR);
					$sth->bindParam(":rg", $info['inf_rg'], PDO::PARAM_STR);
					$sth->bindParam(":cpf", $info['inf_cpf'], PDO::PARAM_STR);
					$sth->bindParam(":ctps", $info['inf_ctps'], PDO::PARAM_STR);
					$sth->bindParam(":endereco", $info['inf_endereco'], PDO::PARAM_STR);
					$sth->bindParam(":cidade", $info['inf_cidade'], PDO::PARAM_STR);
					$sth->bindParam(":uf", $info['inf_uf'], PDO::PARAM_STR);
					$sth->bindParam(":cep", $info['inf_cep'], PDO::PARAM_STR);
					$sth->bindParam(":email", $info['inf_email'], PDO::PARAM_STR);
					$sth->bindParam(":fixo", $info['inf_tel_fixo'], PDO::PARAM_STR);
					$sth->bindParam(":celular", $info['inf_tel_celular'], PDO::PARAM_STR);
					$sth->bindParam(":pis", $info['fnc_pis'], PDO::PARAM_STR);
					$sth->bindParam(":id", $info['inf_id'], PDO::PARAM_INT);
				}
				else
				{
					$sth = $dbh->prepare("UPDATE Funcionario SET inf_nome = :nome, 
										inf_data_nascimento = :nasc, 
										inf_rg = :rg, inf_cpf = :cpf, 
										inf_ctps = :ctps, inf_endereco = :endereco, 
										inf_cidade = :cidade, 
										inf_uf = :uf, inf_cep = :cep, 
										inf_email = :email, 
										inf_tel_fixo = :fixo, 
										inf_tel_celular = :celular, 
										est_curso = :curso, est_periodo = :periodo, 
										est_prev_conclusao = :conclusao, 
										est_termo = :termo, est_empresa = :empresa, 
										est_data_inicio = :inicio, 
										est_data_termino = :termino, 
										est_carga_horaria = :carga, 
										est_cond_estagio = :condicao, 
										est_seguro = :seguro, 
										est_benef_bolsa = :bolsa, 
										est_benef_transporte = :transporte, 
										est_benef_refeicao = :refeicao WHERE inf_id = :id");

					$sth->bindParam(":nome", $info['inf_nome'], PDO::PARAM_STR);
					$sth->bindParam(":nasc", $info['inf_data_nascimento'], PDO::PARAM_STR);
					$sth->bindParam(":rg", $info['inf_rg'], PDO::PARAM_STR);
					$sth->bindParam(":cpf", $info['inf_cpf'], PDO::PARAM_STR);
					$sth->bindParam(":ctps", $info['inf_ctps'], PDO::PARAM_STR);
					$sth->bindParam(":endereco", $info['inf_endereco'], PDO::PARAM_STR);
					$sth->bindParam(":cidade", $info['inf_cidade'], PDO::PARAM_STR);
					$sth->bindParam(":uf", $info['inf_uf'], PDO::PARAM_STR);
					$sth->bindParam(":cep", $info['inf_cep'], PDO::PARAM_STR);
					$sth->bindParam(":email", $info['inf_email'], PDO::PARAM_STR);
					$sth->bindParam(":fixo", $info['inf_tel_fixo'], PDO::PARAM_STR);
					$sth->bindParam(":celular", $info['inf_tel_celular'], PDO::PARAM_STR);
					$sth->bindParam(":curso", $info['est_curso'], PDO::PARAM_STR);
					$sth->bindParam(":periodo", $info['est_periodo'], PDO::PARAM_STR);
					$sth->bindParam(":conclusao", $info['est_prev_conclusao'], PDO::PARAM_STR);
					$sth->bindParam(":termo", $info['est_termo'], PDO::PARAM_STR);
					$sth->bindParam(":empresa", $info['est_empresa'], PDO::PARAM_STR);
					$sth->bindParam(":inicio", $info['est_data_inicio'], PDO::PARAM_STR);
					$sth->bindParam(":termino", $info['est_data_termino'], PDO::PARAM_STR);
					$sth->bindParam(":carga", $info['est_carga_horaria'], PDO::PARAM_STR);
					$sth->bindParam(":condicao", $info['est_cond_estagio'], PDO::PARAM_STR);
					$sth->bindParam(":seguro", $info['est_seguro'], PDO::PARAM_STR);
					$sth->bindParam(":bolsa", $info['est_benef_bolsa'], PDO::PARAM_STR);
					$sth->bindParam(":transporte", $info['est_benef_transporte'], PDO::PARAM_STR);
					$sth->bindParam(":refeicao", $info['est_benef_refeicao'], PDO::PARAM_STR);
					$sth->bindParam(":id", $info['inf_id'], PDO::PARAM_INT);
				}

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function updateFuncionarioInfo($info, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("UPDATE Funcionario SET inf_nome = :nome, 
										inf_data_nascimento = :nasc, 
										inf_rg = :rg, inf_cpf = :cpf, 
										inf_ctps = :ctps, inf_endereco = :endereco, 
										inf_cidade = :cidade, 
										inf_uf = :uf, inf_cep = :cep, 
										inf_email = :email, 
										inf_tel_fixo = :fixo, 
										inf_tel_celular = :celular WHERE inf_id = :id");

					$sth->bindParam(":nome", $info['inf_nome'], PDO::PARAM_STR);
					$sth->bindParam(":nasc", $info['inf_data_nascimento'], PDO::PARAM_STR);
					$sth->bindParam(":rg", $info['inf_rg'], PDO::PARAM_STR);
					$sth->bindParam(":cpf", $info['inf_cpf'], PDO::PARAM_STR);
					$sth->bindParam(":ctps", $info['inf_ctps'], PDO::PARAM_STR);
					$sth->bindParam(":endereco", $info['inf_endereco'], PDO::PARAM_STR);
					$sth->bindParam(":cidade", $info['inf_cidade'], PDO::PARAM_STR);
					$sth->bindParam(":uf", $info['inf_uf'], PDO::PARAM_STR);
					$sth->bindParam(":cep", $info['inf_cep'], PDO::PARAM_STR);
					$sth->bindParam(":email", $info['inf_email'], PDO::PARAM_STR);
					$sth->bindParam(":fixo", $info['inf_tel_fixo'], PDO::PARAM_STR);
					$sth->bindParam(":celular", $info['inf_tel_celular'], PDO::PARAM_STR);
					$sth->bindParam(":id", $info['inf_id'], PDO::PARAM_INT);

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}
		public static function updateFuncionarioSpec($info, $dbh)
		{
			try
			{
				if(isset($info['fnc_pis']))
				{
					$sth = $dbh->prepare("UPDATE Funcionario SET fnc_pis = :pis 
										WHERE inf_id = :id");

					$sth->bindParam(":pis", $info['fnc_pis'], PDO::PARAM_STR);
					$sth->bindParam(":id", $info['inf_id'], PDO::PARAM_INT);
				}
				else
				{
					$sth = $dbh->prepare("UPDATE Funcionario SET est_curso = :curso, 
										est_periodo = :periodo, 
										est_prev_conclusao = :conclusao, 
										est_termo = :termo, est_empresa = :empresa, 
										est_data_inicio = :inicio, 
										est_data_termino = :termino, 
										est_carga_horaria = :carga, 
										est_cond_estagio = :condicao, 
										est_seguro = :seguro, 
										est_benef_bolsa = :bolsa, 
										est_benef_transporte = :transporte, 
										est_benef_refeicao = :refeicao WHERE inf_id = :id");

					
					$sth->bindParam(":curso", $info['est_curso'], PDO::PARAM_STR);
					$sth->bindParam(":periodo", $info['est_periodo'], PDO::PARAM_STR);
					$sth->bindParam(":conclusao", $info['est_prev_conclusao'], PDO::PARAM_STR);
					$sth->bindParam(":termo", $info['est_termo'], PDO::PARAM_STR);
					$sth->bindParam(":empresa", $info['est_empresa'], PDO::PARAM_STR);
					$sth->bindParam(":inicio", $info['est_data_inicio'], PDO::PARAM_STR);
					$sth->bindParam(":termino", $info['est_data_termino'], PDO::PARAM_STR);
					$sth->bindParam(":carga", $info['est_carga_horaria'], PDO::PARAM_STR);
					$sth->bindParam(":condicao", $info['est_cond_estagio'], PDO::PARAM_STR);
					$sth->bindParam(":seguro", $info['est_seguro'], PDO::PARAM_STR);
					$sth->bindParam(":bolsa", $info['est_benef_bolsa'], PDO::PARAM_STR);
					$sth->bindParam(":transporte", $info['est_benef_transporte'], PDO::PARAM_STR);
					$sth->bindParam(":refeicao", $info['est_benef_refeicao'], PDO::PARAM_STR);
					$sth->bindParam(":id", $info['inf_id'], PDO::PARAM_INT);
				}

				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public function updateFuncionarioMatricula($mat, $id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("UPDATE Funcionario SET inf_matricula = :matricula 
										WHERE inf_id = :id");

				$sth->bindParam(":matricula", $mat, PDO::PARAM_STR);
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}

			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		public static function deleteFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("DELETE FROM Funcionario WHERE inf_id = :id LIMIT 1");
				$sth->bindParam(":id", $id, PDO::PARAM_INT);
				
				return $sth->execute();
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
		}
	}
?>