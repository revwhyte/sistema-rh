<?php

	require_once('Database.php');

	class TipoFuncionario
	{
		private $tipo_nome;
		private $tipo_sigla;

		public function __construct($nome, $sigla)
		{
			$this->$tipo_nome = $nome;
			$this->$tipo_sigla = $sigla;
		}

		public function createTipoFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->prepare("INSERT INTO TipoFuncionario(tfn_nome_tipo, tfn_sigla) VALUES(:tipo, :sigla)");
				$sth->bindParam(":tipo", $this->$tipo_nome, PDO::PARAM_STR);
				$sth->bindParam(":sigla", $this->$tipo_sigla, PDO::PARAM_STR);

				return $sth->execute();
			}

			catch (PDOException $e)
			{
				$e->getMessage();
			}
		}

		public static function readTipoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT tfn_nome_tipo, tfn_sigla FROM TipoFuncionario WHERE tfn_id = :id");
				$sth->bindParam(":id", $id, PDO::PARAM_INT);
				$sth->execute();
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $result;
			}

			catch (PDOException $e)
			{
				$e->getMessage();
			}
		}

		public static function readAllTipoFuncionario($dbh)
		{
			try
			{
				$sth = $dbh->prepare("SELECT tfn_id, tfn_nome_tipo, tfn_sigla FROM TipoFuncionario ORDER BY tfn_id ASC");
				$sth->execute();
				$results = $sth->fetchAll(PDO::FETCH_ASSOC);

				return $results;
			}

			catch (PDOException $e)
			{
				$e->getMessage();
			}

		}

		public function updateTipoFuncionario($id, $dbh)
		{
			try
			{
				$sth = $dbh->prepare("UPDATE TipoFuncionario SET tfn_nome_tipo = :tipo, tfn_sigla = :sigla WHERE tfn_id = :id");
				$sth->bindParam(":id", $id, PDO::PARAM_INT);

				return $sth->execute();
			}

			catch (PDOException $e)
			{
				$e->getMessage();
			}
		}

		public static function deleteTipoFuncionario($id, $dbh)
		{
			$sth = $dbh->prepare("DELETE FROM TipoFuncionario WHERE tfn_id = :id LIMIT 1");
			$sth->bindParam(":id", $id, PDO::PARAM_INT);

			return $sth->execute();
		}
	}

?>